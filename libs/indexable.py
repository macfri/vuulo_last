import boto
import logging


class Indexable(object):

    def __init__(self):

        self.cn = boto.connect_cloudsearch()
        self.domain = self.cn.create_domain('products')

        #policy = domain.get_access_policies()
        #policy.allow_search_ip(our_ip)
        #policy.allow_doc_ip(our_ip)

    def save_document(self):
        title = self.domain.create_index_field('title', 'text')
        description = self.domain.create_index_field('description', 'text')
        logging.info(title)
        logging.info(description)
