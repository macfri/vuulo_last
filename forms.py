# -*- coding: utf-8 -*-
import re
from collections import namedtuple

from wtforms import Form, validators, ValidationError
from wtforms.fields import *
from wtforms.fields import Field
from wtforms.widgets import *

from models import Category
from models import Product

#__all__ = ('LoginForm', 'UploadVideoForm', 'RegistrationForm', 'CommentForm')

FIELD_REQ_MSG = 'El campo es requerido.'
FIELD_LEN_MSG = 'El campo debe tener entre %s y %s caracteres.'
FIELD_MIN_LEN_MSG = u'La longitud mínima del campo es de %s caracteres.'

TAG_LEN_MSG = 'Cada tag debe tener entre 3 y 30 caracteres.'
TAG_OVERALL_LEN_MSG = u'El máximo número de caracteres permitidos es 500.'
TAG_RGX_MSG = u'Solamente debe contener caracteres alfanuméricos.'

ALFANUM_RGX = re.compile(r'^\w*[\w|\s]*\w$', re.UNICODE)


def get_name_validator(r):
    return str(r.__class__).split(".")[-1].replace("'>", "")


class DummyField(namedtuple('Field', 'data')):

    def ngettext(self, singular, plural, n):
        return singular if n == 1 else plural


class TagListField(Field):
    widget = TextInput()

    def _value(self):
        if self.data:
            if type(self.data) in (str, unicode):
                return self.data
            elif isinstance(self.data, list):
                return u', '.join(self.data)
        else:
            return u''

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = [x.strip().lower() for x in valuelist[0].split(',')]
        else:
            self.data = []


# ---------------------------------------------------------
# Validators
# ---------------------------------------------------------

class RequiredIfChecked(object):
    def __init__(self, fieldname, message=None):
        self.fieldname = fieldname
        self.message = message

    def __call__(self, form, field):
        try:
            other = form[self.fieldname]
        except KeyError:
            raise validators.ValidationError(u'Campo inválido')

        if other.data and not field.data:
            raise validators.ValidationError(u'El campo es requerido')
        elif not other.data:
            field.errors[:] = []
            raise validators.StopValidation()


class TagOverallLengthValidator(validators.Length):

    def __call__(self, form, field):
        if field.data:
            super(TagOverallLengthValidator, self).__call__(
                    form, DummyField(u', '.join(field.data)))


class TagLengthValidator(validators.Length):

    def __call__(self, form, field):
        if field.data:
            for value in field.data:
                super(TagLengthValidator, self).__call__(
                        form, DummyField(value))


class TagRegexpValidator(validators.Regexp):

    def __call__(self, form, field):
        if field.data:
            for value in field.data:
                super(TagRegexpValidator, self).__call__(
                        form, DummyField(value))


class MultiCheckboxField(SelectMultipleField):
    #widget = ListWidget(prefix_label=False)
    widget = ListWidget(prefix_label=False)
    option_widget = CheckboxInput()
    #option_widget = RadioField(choices=Product.TYPES)


class BaseForm(Form):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)

        if 'excluded' in kwargs:
            for fieldname in kwargs['excluded']:
                del self._fields[fieldname]

        if 'included' in kwargs:
            for fieldname in self._fields.keys():
                if fieldname not in kwargs['included']:
                    del self._fields[fieldname]


class CategoryForm(Form):

    def length(min, max):
        message = 'Must be between %d and %d characters long.' % (min, max)

        def _length(form, field):
            if len(field.data) < min or len(field.data) > max:
                raise ValidationError(message)

        _length.field_flags = ('length_min_%d' % min, 'length_max_%d' % max)
        return _length

    title = TextField(
        label="Tittle *",
        validators=[validators.Required(), length(2, 100)]
        #filters=[lambda x: x.strip() if x else ""]
    )

    status = SelectField(
        validators=[validators.Required()],
        label="Status *",
        choices=(('active', 'active'), ('inactive', 'inactive'))
    )


class ProductForm(Form):

    categories = MultiCheckboxField(
        choices=[(x.get('title'), x.get('title')) \
            for x in Category.objects.all().values('title')]
    )

    def length(min, max):
        message = 'Must be between %d and %d characters long.' % (min, max)

        def _length(form, field):
            if len(field.data) < min or len(field.data) > max:
                raise ValidationError(message)

        _length.field_flags = ('length_min_%d' % min, 'length_max_%d' % max)
        return _length

    title = TextField(
        label="Tittle *",
        validators=[validators.Required(), length(2, 100)]
    )

    description = TextAreaField(
        label='Description',
        validators=[validators.Required(), length(2, 100)],
    )

    cost = IntegerField(
        label='cost',
        validators=[validators.Required()],
    )

    will_travel = IntegerField(
        label='will_travel',
        validators=[validators.Required()],
    )

    allow_retailers = BooleanField(
        label='allow_retailers',
    )

    allow_charities = BooleanField(
        label='allow_charities',
    )

    donate_now = BooleanField(
        label='donate_now',
    )

    status = SelectField(
        validators=[validators.Required()],
        label="Status *",
        #choices=Product.STATUS
    )

    published_at = DateTimeField(
        label="published_at *",
        format='%Y-%m-%d %H:%M',
        validators=[validators.Required()],
    )

    expires_at = DateTimeField(
        label="expires_at *",
        format='%Y-%m-%d %H:%M',
        validators=[validators.Required()],
    )

    type = RadioField(
        "type *",
        choices=Product.TYPES,
        validators=[validators.Required()],
        default='have'
    )

    location = TextField(
        label="location *",
        validators=[validators.Required(), length(2, 100)]
        #filters=[lambda x: x.strip() if x else ""]
    )

    audience = SelectField(
        validators=[validators.Required()],
        label="audience *",
        choices=Product.AUDIENCES_TYPES
    )

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self._fields['status'].choices = [('', '-- Seleccione --')] + \
            list(Product.STATUS)


    user_id = IntegerField(widget=HiddenInput())
    user_id2 = HiddenField()
