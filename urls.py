from tornado.web import url

import controllers.admin.user
import controllers.admin.auth
import controllers.admin.product
import controllers.admin.category


from controllers import api
from controllers import user
from controllers import home
from controllers import match
from controllers import product
from controllers import conversation


admin_handlers = (

    #### admin
    url(
        r'/admin/?',
        controllers.admin.auth.Login,
        name="admin_"
    ),

    #### admin user
    url(
        r'/admin/login',
        controllers.admin.auth.Login,
        name="admin_login"
    ),
    url(
        r'/admin/logout',
        controllers.admin.auth.Logout,
        name="admin_logout"
    ),
    url(
        r'/admin/user',
        controllers.admin.user.List,
        name='admin_user'
    ),

    #### admin category
    url(
        r'/admin/category',
        controllers.admin.category.List,
        name='admin_category'
    ),
    url(
        r'/admin/category/delete',
        controllers.admin.category.Delete,
        name='admin_category_delete'
    ),
    url(
        r'/admin/category/add',
        controllers.admin.category.Add,
        name='admin_category_add'
    ),
    url(
        r'/admin/category/edit/(.*)',
        controllers.admin.category.Edit,
        name='admin_category_edit'
    ),

    #### admin product
    url(
        r'/admin/product',
        controllers.admin.product.List,
        name='admin_product'
    ),
    url(
        r'/admin/product/delete',
        controllers.admin.product.Delete,
        name='admin_product_delete'
    ),
    url(
        r'/admin/product/add',
        controllers.admin.product.Add,
        name='admin_product_add'
    ),
    url(
        r'/admin/product/edit/(.*)',
        controllers.admin.product.Edit,
        name='admin_product_edit'
    ),
)


web_handlers = (

    #### api
    url(
        '/api/login',
        api.Login
    ),
    url(
        '/api/add_product',
        api.AddProduct
    ),

    url(
        '/api/edit_product/(.*)',
        api.EditProduct
    ),


    #### user
    url(
        '/confirmation',
        user.Confirmation,
        name='confirmation'
    ),
    url(
        '/register',
        user.Add,
        name='user'
    ),
    url(
        '/profile',
        user.Edit,
        name='profile'
    ),
    url(
        r"/user/have_products",
        user.HaveProducts,
        name="user_have_products"
    ),
    url(
        r"/user/activate/(\w+)",
        user.Activate,
        name="user_activate"
    ),
    url(
        r"/user/set_password_facebook/(.*)",
        user.SetPasswordFacebook,
        name="user_set_password_facebook"
    ),
    url(
        '/user/login',
        user.Login,
        name='user_login'
    ),
    url(
        '/user/login/facebook',
        user.LoginFacebook,
        name='user_login_facebook'
    ),
    url(
        '/user/logout',
        user.Logout,
        name='user_logout'
    ),
    url(
        '/user/forgot_password',
        user.ForgotPassword,
        name='user_forgot_password'
    ),
    url(
        '/user/change_password/(.*)',
        user.ChangePassword,
        name='user_change_password'
    ),

    #### match
    url(
        '/match/add',
        match.Add,
        name='match_add'
    ),
    url(
        '/match/active',
        match.Active,
        name='match_active'
    ),

    #### conversation
    url(
        '/conversation/reset_unread_comments',
        conversation.ResetUnreadComments,
        name='conversation_reset_unread_comments'
    ),
    url(
        '/conversation/(.*)/send_message_to_owner',
        conversation.SendMessageToOwner,
        name='conversation_send_message_to_owner'
    ),
    url(
        '/conversation/(.*)/send_message_from_owner',
        conversation.SendMessageFromOwner,
        name='conversation_send_message_from_owner'
    ),

    #### product
    url(
        '/product/add',
        product.Add,
        name='product_add'
    ),
    url(
        '/product/edit/(.*)',
        product.Edit,
        name='product_edit'
    ),
    url(
        '/product/delete/(.*)',
        product.Delete,
        name='product_delete'
    ),
    url(
        '/product/(.*)',
        product.View,
        name='product'
    ),

    #### category
    url(
        '/category/(.*)',
        home.Index,
        name='home_category'
    ),

    #### leaderboard
    url(
        '/user_points',
        home.UserPoints,
        name='user_points'
    ),

    #### search
    url(
        '/search',
        home.Search,
        name='search'
    ),

    #### home
    url(
        '/',
        home.Index,
        name='home'
    ),
)

handlers = admin_handlers + web_handlers
