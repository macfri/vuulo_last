#from celery.schedules import crontab
import os
import settings

from worker import WORKER_DIR

CELERY_IMPORTS = ('%s.tasks' % WORKER_DIR ,)
#CELERY_IMPORTS = ('tasks',)
BROKER_TRANSPORT = 'sqs'
BROKER_TRANSPORT_OPTIONS = {
    'region': 'us-east-1',
}

BROKER_URL = 'sqs://%s:%s@' % (settings.AWS_KEY, settings.AWS_SECRET) 

print BROKER_URL

CELERY_ALWAYS_EAGER = False
CELERY_SEND_TASK_SENT_EVENT = False

"""
CELERYBEAT_SCHEDULE = {
    "generate_thumbs": {
        'task': "generate_thumbs",
        'schedule': crontab(minute="*/1"),
    }
}
"""


"""
CELERY_RESULT_BACKEND = 'database'
CELERY_CACHE_BACKEND = 'memcached://127.0.0.1:11211/'
CELERY_RESULT_DBURI = "mysql://root:root@localhost/vuulo_tasks"
"""

CELERY_SEND_TASK_ERROR_EMAILS = True

ADMINS = (
	("funciton", "macfri10@gmail.com"),
)

SERVER_EMAIL = "m4cf1.server@gmail.com"
EMAIL_HOST = "smtp.gmail.com"
EMAIL_PORT = 587
EMAIL_HOST_USER = "m4cf1_123"
EMAIL_HOST_PASSWORD = "m4cf1_123.server@gmail.com"


CELERY_DISABLE_RATE_LIMITS = False


