import settings
import logging
import boto

from models import Product, ApiProduct

#from celery.task import task
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from cStringIO import StringIO

import Image as Image_
from libs.utils import thumbnail


from celery import Celery
BROKER_URL = 'sqs://%s:%s@' % (settings.AWS_KEY, settings.AWS_SECRET) 
#celery = Celery('tasks', broker='amqp://guest@localhost//')
celery = Celery('worker.tasks', broker=BROKER_URL)
celery.conf.CELERY_TASK_SERIALIZER = 'json'


#@task(name='send_email', max_retries=0, ignore_result=True)
@celery.task
def send_email(mail):
    try:
        cn = boto.connect_ses(
            aws_access_key_id=settings.AWS_KEY,
            aws_secret_access_key=settings.AWS_SECRET
        )

        cn.send_email(
            mail.get('sender'),
            mail.get('subject'),
            mail.get('body'),
            mail.get('to'),
            format='html',
        )
    except Exception as exc:
        logging.error(exc)
        send_email.retry(exc=exc)


#@task(name='generate_thumbs', max_retries=2, ignore_result=True)
@celery.task
def generate_thumbs(product_id):

    print "aki voyyyyyyyyy"

    try:
        product = Product.objects.get(product_id)
    except Exception as exc:
        logging.error(exc)
    else:
        cn = S3Connection(
            settings.AWS_KEY, settings.AWS_SECRET
        )

        bucket = cn.get_bucket(settings.S3_BUCKET_NAME)
        key = Key(bucket)
        key.key = '%s/%s' % (settings.S3_UPLOAD_PATH_PRODUCT, product.id)

        try:
            image = Image_.open(StringIO(key.get_contents_as_string()))
        except Exception as exc:
            logging.error(exc)
            generate_thumbs.retry(exc=exc)
        else:

            try:
                for width, height, suffix in  Product.THUMB_SIZES_IMAGES:
                    output_image = StringIO()
                    thumb = thumbnail(image, width, height, force=True,
                            crop=False, adjust_to_width=True)
                    thumb.save(output_image, 'jpeg')

                    key.key = '%s/%s_%s' % (
                        settings.S3_UPLOAD_PATH_PRODUCT, product.id, suffix)

                    key.set_contents_from_string(
                        output_image.getvalue(),
                        headers={'Content-Type': 'image/jpeg'}
                    )
                    key.set_acl('public-read')
            except Exception as exc:
                logging.error(exc)
                generate_thumbs.retry(exc=exc)
            else:
                product.is_upload_thumbs = True

                try:
                    product.update()
                except Exception as exc:
                     logging.error()


#@task(name='generate_thumbs_api', max_retries=2, ignore_result=True)
@celery.task
def generate_thumbs_api(product_id):

    try:
        product = ApiProduct.objects.get(product_id)
    except Exception as exc:
        logging.error(exc)
    else:
        cn = S3Connection(
            settings.AWS_KEY, settings.AWS_SECRET
        )

        bucket = cn.get_bucket(settings.S3_BUCKET_NAME)
        key = Key(bucket)
        key.key = '%s/%s' % (settings.S3_UPLOAD_PATH_PRODUCT_API, product.id)

        try:
            image = Image_.open(StringIO(key.get_contents_as_string()))
        except Exception as exc:
            logging.error(exc)
            generate_thumbs_api.retry(exc=exc)
        else:

            try:
                for width, height, suffix in  ApiProduct.THUMB_SIZES_IMAGES:
                    output_image = StringIO()
                    thumb = thumbnail(image, width, height, force=True,
                            crop=False, adjust_to_width=True)
                    thumb.save(output_image, 'jpeg')

                    key.key = '%s/%s_%s' % (
                        settings.S3_UPLOAD_PATH_PRODUCT_API,
                            product.id, suffix)

                    key.set_contents_from_string(
                        output_image.getvalue(),
                        headers={'Content-Type': 'image/jpeg'}
                    )
                    key.set_acl('public-read')
            except Exception as exc:
                logging.error(exc)
                generate_thumbs_api.retry(exc=exc)
            else:
                product.is_upload_thumbs = True

                try:
                    product.update()
                except Exception as exc:
                    logging.error()


#@task(name='generate_thumbs_queue', max_retries=0)
def generate_thumbs_queue():
    from boto.sqs.connection import SQSConnection
    conn = SQSConnection(settings.AWS_KEY, settings.AWS_SECRET)
    q = conn.create_queue('generate_thumbs')
    messages = q.get_messages()
    #visibility_timeout=60

    if len(messages) > 0:
        for m in messages:

            try:
                product = Product.objects.get(m.get_body())
            except Exception as exc:
                logging.error(exc)
            else:
                cn = S3Connection(
                    settings.AWS_KEY, settings.AWS_SECRET
                )

                bucket = cn.get_bucket(settings.S3_BUCKET_NAME)
                key = Key(bucket)
                key.key = '%s/%s' % (
                    settings.S3_UPLOAD_PATH_PRODUCT, product.id)

                try:
                    image = Image_.open(StringIO(key.get_contents_as_string()))
                except Exception as exc:
                    logging.error(exc)
                    #generate_thumbs.retry(exc=exc)
                else:

                    try:
                        for width, height, suffix in \
                            Product.THUMB_SIZES_IMAGES:
                            output_image = StringIO()
                            thumb = thumbnail(image, width, height, force=True,
                                    crop=False, adjust_to_width=True)
                            #thumb.save(output_image, 'jpeg')

                            key.key = '%s/%s_%s' % (
                                settings.S3_UPLOAD_PATH_PRODUCT,
                                product.id, suffix)

                            key.set_contents_from_string(
                                output_image.getvalue(),
                                headers={'Content-Type': 'image/jpeg'}
                            )
                            key.set_acl('public-read')
                    except Exception as exc:
                        logging.error(exc)
                        #generate_thumbs.retry(exc=exc)
                    else:
                        q.delete_message(m)

            #m.get_body()

if __name__ == '__main__':
    #generate_thumbs('7d20cf73-1e96-429e-bbc7-5820af5d4ede')
    #generate_thumbs()
    logging.info('main')
