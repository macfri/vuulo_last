import uuid
import logging

import settings
import simpledb

from simpledb import models
from datetime import datetime, timedelta

from boto.s3.key import Key
from boto.s3.connection import S3Connection

from StringIO import StringIO
from libs.utils import crypt_password, generate_id, slugify
from dateutil.relativedelta import relativedelta

log = logging.getLogger(__name__)

FORMAT_DATE = '%Y-%m-%d %H:%M:%S'

DOMAINS = dict(
     domain_user='user',
     domain_match='match',
     domain_points='points',
     domain_comment='comment',
     domain_product='product',
     domain_category='category',
     domain_api_user='api_user',
     domain_system_user='system_user',
     domain_api_product='api_product',
     domain_conversation='conversation')

AW_SDB = simpledb.SimpleDB(
    settings.AWS_KEY,
    settings.AWS_SECRET
)

AW_S3 = S3Connection(
    settings.AWS_KEY,
    settings.AWS_SECRET
)


class SystemUser(models.Model):

    STATUS = (('active', 'active'),
        ('inactive', 'inactive'))

    __id = models.ItemName()
    username = models.Field(required=True)
    password = models.Field(required=True)
    email = models.Field(required=True)
    status = models.Field(default='active')
    created_at = models.DateTimeField(format=FORMAT_DATE,
        default=datetime.now())
    modified_at = models.DateTimeField(format=FORMAT_DATE)
    last_login_at = models.DateTimeField()

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_system_user')

    @classmethod
    def email_exists(self, email):
        rs = self.objects.filter(
            simpledb.item_name(eq=email)
        )
        return True if len(rs) > 0 else False

    @classmethod
    def auth(self, email, password):
        return True if len(self.objects.filter(
            simpledb.where(email=email) & \
            simpledb.where(password=crypt_password(password)) & \
            simpledb.where(status='active')
        )) > 0 else False

    def set_password(self, value):
        if isinstance(value, unicode):
            value = value.encode('utf8')
        self.password = crypt_password(value)

    def save(self, **kwargs):
        if self.password:
            self.password = crypt_password(self.password)
        super(User, self).save(**kwargs)


class User(models.Model):

    STATUS = (('active', 'active'),
        ('inactive', 'inactive'))

    email = models.ItemName()
    name = models.Field(required=True)
    lastname = models.Field(required=True)
    gender = models.Field(required=True)
    password = models.Field(default='')
    status = models.Field(default='inactive')
    created_at = models.DateTimeField(default=datetime.now(),
        format=FORMAT_DATE)
    modified_at = models.DateTimeField(default=datetime.now(),
        format=FORMAT_DATE)
    last_login_at = models.DateTimeField()
    facebook_id = models.Field(default='')
    activation = models.Field(default='')
    activation_passwd = models.Field(default='')
    activation_passwd_facebook = models.Field(default='')
    zip_code = models.Field(default='')
    favorite_charity = models.Field(default='')
    email_notification = models.BooleanField(default=False)

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_user')

    @property
    def upload_path(self):
        return settings.S3_UPLOAD_PATH_USER

    @property
    def upload_url(self):
        return settings.S3_UPLOAD_URL_USER

    @property
    def photo(self):
        return '%s%s' % (
            self.upload_url,
            self.email
        )

    @property
    def comments(self):
        try:
            return Comment.objects.filter(
                simpledb.every(user=self.id)
            )
        except Exception as exc:
            log.error(exc)
            return None

    @property
    def matches(self):
        try:
            return Match.objects.filter(
                simpledb.every(user=self.id)
            )
        except Exception as exc:
            log.error(exc)
            return None

    @property
    def matches_want(self):
        data = []
        for x in Match.objects_want(self.id):
            product = Product.objects.get(x.target)
            data.append(dict(title=product.title,
                image=product.image, slug=product.slug))
        return data

    @property
    def matches_have(self):
        data = []
        for x in Match.objects_have(self.id):
            product = Product.objects.get(x.source)
            data.append(dict(title=product.title,
                image=product.image, slug=product.slug))
        return data

    @property
    def points(self):
        try:
            return Points.objects.get(
                self.email
            )
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def auth(self, email, password):

        return True if len(self.objects.filter(
            simpledb.item_name(eq=email) & \
            simpledb.where(password=crypt_password(password)) & \
            simpledb.where(status='active')
        )) > 0 else False

    @classmethod
    def email_exists(self, email):
        rs = self.objects.filter(
            simpledb.item_name(eq=email)
        )
        return True if len(rs) > 0 else False

    @classmethod
    def get(self, email):
        try:
            return User.objects.get(email)
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def get_by_activation(self, token):
        try:
            return User.objects.filter(
                activation=token)[0]
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def get_by_activation_passwd(self, token):
        try:
            return User.objects.filter(
                activation_passwd=token)[0]
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def get_by_activation_passwd_facebook(self, token):
        try:
            return User.objects.filter(
                activation_passwd_facebook=token)[0]
        except Exception as exc:
            log.error(exc)
            return None

    @property
    def have_products(self):
        try:
            return Product.objects.filter(
                type='have',
                status='active',
                user=self.email
            )
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def data_user(self, email):
        try:
            return self.objects.get(
                email,
                consistent_read=True
            )
        except Exception as exc:
            log.error(exc)
            return None

    def set_password(self, password):
        self.password = crypt_password(password)

    def generate_token(self, field):
        while True:
            activation = ('%s%s' % (
                settings.COOKIE_SECRET,
                uuid.uuid4().hex))[::-1]
            try:
                if self.objects.filter(
                    field=activation).count() == 0:
                    activation = activation
                    break
            except Exception as exc:
                log.error(exc)
                break
        return activation

    def save(self, **kwargs):
        self.password = self.crypt_password(self.password)
        self.activation = self.generate_token(
            'activation')
        self.activation_passwd = self.generate_token(
            'activation_passwd')
        self.activation_passwd_facebook = self.generate_token(
            'activation_passwd_facebook')
        super(User, self).save(**kwargs)

    def update(self, **kwargs):
        self.modified_at = datetime.now()
        super(User, self).save(**kwargs)

    def upload_img(self, body):
        try:
            fp = StringIO(body)
            bucket = AW_S3.get_bucket(
                settings.S3_BUCKET_NAME)

            key = Key(bucket)
            key.key = '%s/%s' % (
                self.upload_path,
                self.email
            )
            key.set_contents_from_file(
                fp, headers={'Content-Type': 'image/jpeg'}
            )
            key.set_acl('public-read')

            return 'http://%s.%s/%s' % (
                bucket.name,
                settings.S3_IMAGES_URL,
                key.key
            )
        except Exception as exc:
            log.error(exc)
            return None


class Category(models.Model):

    STATUS = (('active', 'active'),
        ('inactive', 'inactive'))

    slug = models.ItemName()
    title = models.Field(required=True)
    status = models.Field(default='inactive')
    created_at = models.DateTimeField(format=FORMAT_DATE,
        default=datetime.now())
    last_modified_at = models.DateTimeField(format=FORMAT_DATE)
    total_products = models.NumberField(default=0)
    total_products_active = models.NumberField(default=0)
    total_products_inactive = models.NumberField(default=0)
    total_products_have = models.NumberField(default=0)
    total_products_want = models.NumberField(default=0)

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_category')

    @property
    def products(self):
        return Product.objects.filter(
            categories=self.slug
        )

    @classmethod
    def get(self, id):
        try:
            return self.objects.get(id)
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def _generate_slug(cls, title):
        _count = 2
        _slug = slugify(title)
        while len(cls.objects.filter(slug=_slug)) > 0:
            _slug = slugify(u'%s %s' % (title, _count))
            _count += 1
        return _slug

    def save(self, **kwargs):
        self.slug = self._generate_slug(self.title)
        super(Category, self).save(**kwargs)

    def update(self, **kwargs):
        super(Category, self).save(**kwargs)

    def delete(self):
        for item in self.products:
            index = item.categories.index(self.slug)
            del item.categories[index]
            item.update()
        super(Category, self).delete()


class BaseProduct(models.Model):

    SHOW_FORMAT_DATE = '%A, %b. %d - %I:%M%p'
    REMAINS_SHORT_TIME = '%dd %dhr'

    DURATION_POST = (
        ('7', '1 Week'),
        ('14', '2 Weeks'),
        ('21', '3 Weeks')
    )

    THUMB_SIZES_IMAGES = (
        (640, 428, 'large'),
        (377, 286, 'medium'),
        (165, 132, 'small'),
        (88, 69, 'tiny'),
    )

    TYPES = (('have', 'have'),
        ('want', 'want'))

    STATUS = (('available', 'available'),
        ('unavailable', 'unavailable'))

    AUDIENCES_TYPES = (('all', 'all'),
                        ('local', 'local'),
                        ('private group', 'private group'))

    __id = models.ItemName()
    slug = models.Field(required=True)
    title = models.Field(required=True)
    description = models.Field(required=True)
    cost = models.Field(required=True)
    duration = models.NumberField(required=True)
    category = models.Field(required=True)
    type = models.Field(required=True)
    will_travel = models.NumberField(default=0)
    allow_retailers = models.BooleanField(default=False)
    allow_charities = models.BooleanField(default=False)
    donate_now = models.BooleanField(default=False)
    status = models.Field(required=True, default='unavailable')
    created_at = models.DateTimeField(format=FORMAT_DATE,
        default=datetime.now())
    last_modified_at = models.DateTimeField(format=FORMAT_DATE)
    published_at = models.DateTimeField(format=FORMAT_DATE)
    location = models.Field()
    audience = models.Field(default='')
    is_upload_thumbs = models.BooleanField(default=False)
    user = models.Field()

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_product')

    @property
    def id(self):
        return self.__id

    @property
    def upload_path(self):
        return settings.S3_UPLOAD_PATH_PRODUCT

    @property
    def upload_url(self):
        return settings.S3_UPLOAD_URL_PRODUCT

    @property
    def image(self):
        return '%s%s' % (
            self.upload_url,
            self.id
        )

    @property
    def tiny_url(self):
        return '%s_%s' % (self.image, 'tiny')

    @property
    def small_url(self):
        return '%s_%s' % (self.image, 'small')

    @property
    def medium_url(self):
        return '%s_%s' % (self.image, 'medium')

    @property
    def large_url(self):
        return '%s_%s' % (self.image, 'large')

    @property
    def expired(self):
        return True if self.expires_at >= datetime.now() else False

    @property
    def expires_at(self):
        return self.published_at + timedelta(days=self.duration)

    @property
    def expires_at_remains(self):
        diff = relativedelta(self.expires_at, datetime.now())
        return self.REMAINS_SHORT_TIME % (diff.days, diff.hours)

    @property
    def expires_at_format(self):
        return self.expires_at.strftime(self.SHOW_FORMAT_DATE)

    @property
    def data_category(self):
        try:
            return Category.objects.get(self.category)
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def get(self, id):
        try:
            return self.objects.get(id)
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def get_by_slug(self, slug):
        try:
            return self.objects.filter(slug=slug)[0]
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def _generate_slug(cls, title):
        _count = 2
        _slug = slugify(title)
        while len(cls.objects.filter(slug=_slug)) > 0:
            _slug = slugify(u'%s %s' % (title, _count))
            _count += 1
        return _slug

    def upload_img(self, body):
        try:
            fp = StringIO(body)
            bucket = AW_S3.get_bucket(
                settings.S3_BUCKET_NAME)

            key = Key(bucket)
            key.key = '%s/%s' % (
                self.upload_path,
                self.id
            )
            key.set_contents_from_file(
                fp, headers={'Content-Type': 'image/jpeg'}
            )
            key.set_acl('public-read')

            return 'http://%s.%s/%s' % (
                bucket.name,
                settings.S3_IMAGES_URL,
                key.key
            )
        except Exception as exc:
            log.error(exc)
            return None

    def save(self, **kwargs):
        self.__id = generate_id()
        self.slug = self._generate_slug(self.title)
        super(BaseProduct, self).save(**kwargs)

    def update(self, **kwargs):
        self.last_modified_at = datetime.now()
        super(BaseProduct, self).save(**kwargs)

    def delete(self, **kwargs):
        super(BaseProduct, self).delete(**kwargs)


class Product(BaseProduct):

    @property
    def data_user(self):
        return User.data_user(self.user)

    @property
    def conversations(self):
        return Conversation.get_conversations_by_product(
            self.id)

    @property
    def notifications(self):
        data = []
        for x in Match.notifications(self.id):
            product_have = Product.objects.get(x.target)
            data.append(dict(
                id=x.id, title=product_have.title,
                image=product_have.image,
                user=product_have.data_user.name))
        return data

    @property
    def matches_want(self):
        data = []
        for x in Match.objects_want(self.id):
            product = Product.objects.get(x.target)
            data.append(dict(title=product.title,
                image=product.image, slug=product.slug))
        return data

    @property
    def matches_have(self):
        data = []
        for x in Match.objects_have(self.id):
            product = Product.objects.get(x.source)
            data.append(dict(title=product.title,
                image=product.image, slug=product.slug))
        return data

    def is_owner(self, user):
        return True if user.email == self.user else False

    def get_conversation(self, with_user):
        return Conversation.get_conversation_with_user(
            self.id,
            with_user)

    def delete_conversations(self):
        for conversation in self.conversations:
            try:
                conversation.delete()
            except Exception as exc:
                log.error(exc)

    def delete_matches_have(self):
        for match in Match.objects_have(self.id):
            log.info(match)
            try:
                match.delete()
            except Exception as exc:
                log.error(exc)

    def delete_matches_want(self):
        for match in Match.objects_want(self.id):
            log.info(match)
            try:
                match.delete()
            except Exception as exc:
                log.error(exc)

    def delete_images(self):
        suffixs = ['_%s' % x[2] for x in \
            self.THUMB_SIZES_IMAGES]
        suffixs.insert(0, '')

        for suffix in suffixs:
            try:
                key = AW_S3.get_bucket(
                    settings.S3_BUCKET_NAME).get_key(
                    '%s/%s%s' % (
                        settings.S3_UPLOAD_PATH_PRODUCT,
                        self.id, suffix)
                    )
                key.delete()
            except Exception as exc:
                log.error(exc)

    def save_totales(self, _categories, _type=None, _status=None, is_new=True):

        for id in _categories:
            try:
                cat = Category.objects.get(id)
            except Exception as exc:
                log.error(exc)
            else:

                if is_new:
                    cat.total_products = int(
                        cat.total_products) + 1

                if not is_new and _type:

                    if _type != self.type:

                        if self.type == 'want':
                            cat.total_products_want = int(
                                cat.total_products_want) + 1

                            cat.total_products_have = int(
                                cat.total_products_have) - 1
                        else:
                            cat.total_products_have = int(
                                cat.total_products_have) + 1
                            cat.total_products_want = int(
                                cat.total_products_want) - 1

                else:

                    if self.type == 'want':
                        cat.total_products_want = int(
                            cat.total_products_want) + 1
                    else:
                        cat.total_products_have = int(
                            cat.total_products_have) + 1

                if not is_new and _status:

                    if _status != self.status:

                        if self.status == 'active':
                            cat.total_products_active = int(
                                cat.total_products_active) + 1

                            cat.total_products_inactive = int(
                                cat.total_products_inactive) - 1
                        else:
                            cat.total_products_inactive = int(
                                cat.total_products_inactive) + 1

                            cat.total_products_active = int(
                                cat.total_products_active) - 1
                else:

                    if self.status == 'active':
                        cat.total_products_active = int(
                            cat.total_products_active) + 1
                    else:
                        cat.total_products_inactive = int(
                            cat.total_products_inactive) + 1

            try:
                cat.update()
            except Exception as exc:
                log.error(exc)

    def delete(self, **kwargs):
        self.delete_conversations()
        self.delete_matches_want()
        self.delete_matches_have()
        self.delete_images()
        super(Product, self).delete(**kwargs)


class Conversation(models.Model):

    STATUS = (('active', 'active'),
        ('inactive', 'inactive'))

    TYPES = ('blog', 'private')

    __id = models.ItemName()
    product = models.Field(required=True)
    status = models.Field(default='disabled')
    created_at = models.DateTimeField(default=datetime.now(),
        format=FORMAT_DATE)
    last_modified_at = models.DateTimeField(format=FORMAT_DATE)
    unread_comments = models.NumberField(default=0)
    with_user = models.Field(required=True)
    type = models.Field(default='')

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_conversation')

    @property
    def id(self):
        return self.__id

    @property
    def user_data(self):
        return User.objects.get(self.with_user)

    @property
    def get_conversation(self, id):
        return Comment.objects.filter(
            conversation=self.id
        )

    @classmethod
    def get_conversation_with_user(self, id, with_user):
        try:
            return self.objects.filter(
                product=id, with_user=with_user)[0]
        except Exception as exc:
            log.warn(exc)
            return None

    @property
    def comments(self):
        return Comment.objects.filter(
            conversation=self.id
        )

    @classmethod
    def get(self, id):
        try:
            return self.objects.get(id)
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def get_conversations_by_product(self, id):
        return self.objects.filter(
            product=id)

    @classmethod
    def tot_notifications_type(self, user, type):
        news = 0
        for product in Product.objects.filter(user=user):
            conversations = self.objects.filter(
                product=product.id, type=type)
            if conversations:
                news += 1
        return news

    def save(self, **kwargs):
        self.__id = generate_id()
        super(Conversation, self).save(**kwargs)

    def update(self, **kwargs):
        self.last_modified_at = datetime.now()
        super(Conversation, self).save(**kwargs)

    def increase_unread_comments(self):
        self.unread_comments = int(
            self.unread_comments) + 1


class Comment(models.Model):

    STATUS = (('active', 'active'),
        ('inactive', 'inactive'))

    __id = models. ItemName()
    description = models.Field(required=True)
    conversation = models.Field(required=True)
    status = models.Field(default='inactive')
    created_at = models.DateTimeField(default=datetime.now(),
        format=FORMAT_DATE)
    published_at = models.DateTimeField(format=FORMAT_DATE)
    user = models.Field(required=True)

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_comment')

    @property
    def id(self):
        return self.__id

    @property
    def created_at_f(self):
        try:
            return self.created_at.strftime(
                '%Y-%m-%d %H:%M:%S')
        except Exception as exc:
            log.error(exc)
            return ''

    @property
    def user_data(self):
        return User.objects.get(self.user)

    def save(self, **kwargs):
        self.__id = generate_id()
        super(Comment, self).save(**kwargs)

    def update(self, **kwargs):
        super(Comment, self).save(**kwargs)


class Match(models.Model):

    STATUS = (('active', 'active'),
        ('inactive', 'inactive'))

    TYPES = (('normal', 'normal'),
        ('match', 'match'))

    __id = models.ItemName()
    source = models.Field(required=True)
    target = models.Field(required=True)
    status = models.Field(default='inactive')
    user = models.Field(required=True)
    type = models.Field(default='normal')
    created_at = models.DateTimeField(default=datetime.now(),
        format=FORMAT_DATE)

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_match')

    @property
    def id(self):
        return self.__id

    @classmethod
    def get(self, id):
        try:
            return self.objects.get(id)
        except Exception as exc:
            log.error(exc)
            return None

    @classmethod
    def objects_want(self, id):
        return self.objects.filter(
            source=id)

    @classmethod
    def objects_have(self, id):
        return self.objects.filter(
            target=id)

    @classmethod
    def notifications(self, id):
        return Match.objects.filter(
            source=id, status=self.STATUS[0])

    def save(self, **kwargs):
        self.__id = generate_id()
        super(Match, self).save(**kwargs)

    def update(self, **kwargs):
        super(Match, self).save(**kwargs)


class Points(models.Model):

    user = models.ItemName()
    matching = models.Field(default='0')
    charity = models.Field(default='0')
    products = models.Field(default='0')
    total = models.Field(default='0')
    created_at = models.DateTimeField(default=datetime.now(),
        format=FORMAT_DATE)
    last_modified_at = models.DateTimeField(format=FORMAT_DATE)

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_points')

    @classmethod
    def email_exists(self, email):
        rs = self.objects.filter(
            simpledb.item_name(eq=email)
        )
        return True if len(rs) > 0 else False

    @property
    def user_data(self):
        return User.objects.get(self.user)

    def save_per_type(self, type):

        points = settings.POINTS.get(type)

        if not self.email_exists(self.user):

            if type == 'product':
                self.products = points

            if type == 'charity':
                self.charity = points

            if type == 'matching':
                self.matching = points

            self.total = points
            self.save()

        else:

            if type == 'product':
                self.products = int(self.products) + points

            if type == 'charity':
                self.charity = int(self.charity) + points

            if type == 'matching':
                self.matching = int(self.matching) + points

            self.total = self.total + points
            self.update()

    def save(self, **kwargs):
        super(Points, self).save(**kwargs)

    def update(self, **kwargs):
        self.last_modified_at = datetime.now()
        super(Points, self).save(**kwargs)


class ApiUser(models.Model):

    email = models.ItemName()
    password = models.Field(default='')
    token = models.Field(default='')
    last_login_at = models.DateTimeField(default=datetime.now(),
        format=FORMAT_DATE)
    expiration_date = models.Field()

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_api_user')

    @classmethod
    def email_exists(self, email):
        rs = self.objects.filter(
            simpledb.item_name(eq=email)
        )
        return True if len(rs) > 0 else False

    @classmethod
    def auth(self, email, password):
        return True if len(self.objects.filter(
            simpledb.item_name(eq=email) & \
            simpledb.where(password=crypt_password(password))
        )) > 0 else False

    @classmethod
    def get_user_by_token(self, token):
        try:
            user = self.objects.filter(
                token=token)[0]
        except Exception as exc:
            log.error(exc)
            user = None
        return user

    @classmethod
    def generate_token(self):
        while True:
            token = ('%s%s' % (
                settings.COOKIE_SECRET,
                uuid.uuid4().hex))[::-1]
            try:
                if self.objects.filter(
                    token=token).count() == 0:
                    token = token
                    break
            except Exception as exc:
                log.error(exc)
                break
        return token

    def has_expires(self):
        expiration_date = datetime.strptime(
            self.expiration_date, '%Y-%m-%dT%H:%M:%S')
        if not expiration_date > datetime.now():
            return True
        else:
            return False

    def save(self, **kwargs):
        self.password = crypt_password(self.password)
        super(ApiUser, self).save(**kwargs)

    def update(self, **kwargs):
        self.last_login_at = datetime.now()
        super(ApiUser, self).save(**kwargs)


class ApiProduct(BaseProduct):

    class Meta:
        connection = AW_SDB
        domain = DOMAINS.get('domain_api_product')

    @property
    def upload_path(self):
        return settings.S3_UPLOAD_PATH_PRODUCT_API

    @property
    def upload_url(self):
        return settings.S3_UPLOAD_URL_PRODUCT_API
