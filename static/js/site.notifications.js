'use strict';

!function($) {
  function showNotification() {
  }

  function init() {
    $('.js-notifications').on('click', showNotification);
  }

  $(init);
}(jQuery);
