'use strict';

!function($) {
  var login_form

  function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\ ".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA -Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  function submit(e) {
    e.preventDefault();

    var form = $(e.currentTarget);
    var email = $('#email', form).removeClass('error');
    var password = $('#password', form).removeClass('error');
    var data = { email: email.val(), password: password.val() };
    var login_message = $('#login_message', form).empty();
    var status = 0;

    if (data.email === '') {
      status |= 1;
    }

    else if (!validateEmail(data.email)) {
      status |= 4;
    }

    if (data.password === '') {
      status |= 2;
    }

    if (status !== 0) {
      if ((status & 1) === 1 || (status & 4) === 4) {
        email.addClass('error');
      }

      if ((status & 2) === 2) {
        password.addClass('error');
      }
      return;
    }

    form.addClass('wait');

    $.ajax({
      type: 'post',
      dataType: 'json',
      url: form.attr('action'),
      data: data,
      complete: function() {
      },
      success: function(r) {
        if (r && r.status_code === 0) {
          window.location.href = form.attr('data-redirect');
        }

        else {
          if (r.status_code == 1) {
            login_message.html('<p>Fill data</p>')
          }

          else if (r.status_code == 2) {
            login_message.html('<p>Auth failed</p>')
          }
          form.removeClass('wait');
        }
      },
      error: function() {
        login_message.html('<p>Auth failed</p>')
        form.removeClass('wait');
      }
    });
  }

  function init() {
    login_form = $('#form_login');
    login_form.on('submit', submit); 
  }

  $(init);
}(jQuery);
