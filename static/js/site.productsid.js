!function($) {

  $(function() {
    var product_1 = location.search.split('?id=')[1];

    $(".product").click(function(e) {
      e.preventDefault();
      var user_have_product = $(this).attr("data-id");
     
      window.parent.match.send({
          product_1: product_1,   
          product_2: user_have_product,
          wait: 'true'
      })
    });
  });

}(jQuery);
