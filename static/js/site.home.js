'use strict';

!function($) {
  var filter = {
    selectItem: function(e) {
      var self, el, cls, target, url, data;

      self = this;
      el = $(e.currentTarget);
      url = el.attr('data-href');

      if (!url) {
        return;
      }

      cls = '';
      target = el.parents('.wheel');

      el.parents()
        .find('.active').removeClass('active');

      el.addClass('active');

      if (url.indexOf('want') !== -1) {
        cls = 'want';
      }
      else {
        cls = 'have';
      }

      target.addClass('loading');

      $.ajax({
        url: url,
        dataType: 'json',
        type: 'get',
        complete: function() {
          target.removeClass('loading');
        },
        success: function(r) {
          data = [];

          if (r && r.data) {
            $.each(r.data, function(i, o) {
              if (o.categories) {
                o.categories = o.categories.join(' ');
              }
              else {
                o.categories = '';
              }

              data.push({
                slug: o.slug,
                categories: o.categories,
                username: o.username,
                description: o.description,
                id: o.id,
                pay: o.cost,
                travel: o.will_travel,
                expire: o.expire_at,
                image: o.image,
                title: o.title
              });
            });

            if (cls === 'want') {
              _want_products = data;
              wheels.reinit();
            }

            else if (cls === 'have') {
              _have_products = data;
              wheels.reinit();
            }
          }

          else {
          }
        },

        error: function() {
        }
      });

      el.parents('.filter').hide();
    },

    init: function() {
      var self = this;

      $('.filters')
        .on('click', '.js-filter-item', $.proxy(self, 'selectItem'));
    }
  };

  var match = {
    send: function(data) {
      data = data || {};
      var loading =  app.dialog.create({ title: '', content: 'Loading', width: 300, height: 70 });

      return $.ajax({
        type: 'get',
        dataType: 'json',
        url: '/product/match',
        data: data,
        complete: function() {
          loading.dialog('close');
        },
        success: function(r) {
          if (r && r.status_code === 0) {
            app.dialog.acceptOr({
              title: 'Match',
              content: 'Match done',
              width: 350,
              height: 'auto'
            });
          }

          else if (r && r.status_code === 3) {
            // mach realizado
            app.dialog.acceptOr({
              title: 'Match',
              content: 'Match done',
              width: 350,
              height: 'auto'
            });
          }

          else  {
            // no se actualizo puntaje de usuario
            app.dialog.acceptOr({
              title: 'Match',
              content: 'March failed',
              width: 350,
              height: 'auto'
            });
          }
        }
      });
    },

    match: function(e) {
      e.preventDefault();

      var self = this;
      var want = $('#want_product').scroller('getValue');
      var have = $('#have_product').scroller('getValue');

      if (have && want) {
        self.send({
          product_1: _want_products[want[0]].id,
          product_2: _have_products[have[0]].id
        });
      }
    },

    init: function() {
      var self = this;

      self.trigger = $('#main-match');

      if (!self.trigger.is('.js-login-form')) {
        self.trigger.on('click', $.proxy(self, 'match'));
      }
    }
  };
  window.match = match;

  var wheels = {
    tpl: {
      item: ''
    },

    showDetail: function(e) {
      var self = this;
      var el = $(e.currentTarget);

      el.parent().find('.hover').removeClass('hover');

      if (e.type === 'mouseenter') {
        el.addClass('hover');
      }
    },

    create: function(config) {
      var wheels, wheel, el, data, self;
      config = config || {};

      if (!config.data) {
        return;
      }

      self = this;
      wheels = [{}];
      wheel = {};

      if (config.cls === 'have') {
        data = _have_products;
      }

      else if (config.cls === 'want') {
        data = _want_products;
      }

      $.each(config.data, function(i, o) {
        var d = data[i];

        wheel[i] = self.tpl.item

          .split('%class%').join(config.cls)

          .split('%index%').join(i)

          .split('%image%').join(o.image)

          .split('%slug%').join(o.slug)

          .split('%categories%').join(d.categories)

          .split('%title%').join(d.title)

          //.split('%description%').join(d.description)

          .split('%pay%').join(d.pay)

          .split('%travel%').join(d.travel)

          .split('%expire%').join(d.expire)

          .split('%id%').join(d.id)

          .split('%classbtn%')
            .join((config.cls === 'have') ? 'wantthis' : 'havethis')
      });

      wheels[0]['product'] = wheel;

      el = $(config.el);

      el.scroller({
        theme: 'ios',
        display: 'inline',
        mode: 'scroller',
        wheels: wheels,
        //height: 117,
        height: 202,
        rows: 3,
        delay: 500
      });

      if (config.data.length > 3) {
        el.scroller('setValue', [3]);
      }
      else if (config.data.length === 3) {
        el.scroller('setValue', [1]);
      }
    },

    init: function() {
      var self = this;
      var content = $('#home');

      // templates
      self.tpl.item = $('#product_item').html();

      // detalles
      content
        .on('mouseenter mouseleave', '.dw-v', $.proxy(self, 'showDetail'));

      self.reinit();
    },

    reinit: function() {
      var self = this;
      var want = $('#want_product');
      var have = $('#have_product');

      want.scroller('destroy');
      have.scroller('destroy');

      // want wheel
      self.create({
        el: '#want_product',
        data: _want_products,
        cls: 'want'
      });

      // have wheel
      self.create({
        el: '#have_product',
        data: _have_products,
        cls: 'have'
      });
    }
  };

  var detail = {
    showDetail: function(e) {
      e.preventDefault();
      var self = this;
      var el = $(e.currentTarget);
      var m = app.dialog.create({
        title: '',
        content: '<iframe class="detail-frame" src="' + el.attr('href') +
        '" frameborder="0" height="500" width="884"></iframe>' 
      });
    },

    init: function() {
      var self = this;
      $('body').on('click', '.js-show-detail', $.proxy(self, 'showDetail'));
      $('body').on('click', '.havethis', $.proxy(self, 'showDetail'));
      $('body').on('click', '.wantthis', $.proxy(self, 'showDetail'));
    }
  };

  var modal = {
    init: function() {
      var el = $('.js-modal-launch');
    }
  };

  $(function() {
    filter.init();
    match.init();
    wheels.init();
    detail.init();
    modal.init();
  });

}(jQuery);
