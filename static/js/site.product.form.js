!function($) {
  var form = {
    init: function() {
      var self = this;
      self.el = $('#form');

      self.el.find('#published_at').datepicker({
        dateFormat: 'yy-mm-dd'
      });
      self.el.find('#expires_at').datepicker({
        dateFormat: 'yy-mm-dd'
      });
    }
  };

  $(function() {
    form.init();
  });
}(jQuery);
