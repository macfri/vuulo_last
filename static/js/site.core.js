'use strict';


!function() {
  // -------------------------------------------------------
  // showMe
  // -------------------------------------------------------
  function ShowMe() {
    return this.each(function() {
      var el = $(this);
      el.on('click', toggleShow);
    });

    function toggleShow(e) {
      e.preventDefault();

      var el = $(e.currentTarget);
      var t = $('#' + el.attr('href').split('#')[1]);

      if (!el.attr('target-known')) {
        el.attr('target-known', 1);
        t.addClass('showmetarget');
      }

      if (t.is(':visible')) {
        t.hide();
      }

      else {
        t.show();
        $('.showmetarget').not(t).hide();
      }
    }
  }

  $.fn.showMe = ShowMe;

  function ShowMeEveryBody(e) {
    var el = $(e.target);
    var elp = el.parents('.js-showme');

    if (
      elp.length === 0 &&
      el.hasClass('js-showme') === false &&
      el.parents('.showmetarget').length === 0
    ) {
      $('.showmetarget').hide();
    }
  }

  // -------------------------------------------------------
  // placeholder
  // -------------------------------------------------------
  function placeHolder() {
    return this.each(function() {
      $(this).on({
        'focus': function() {
          var el = $(this);
          if (el.val() === el.attr('placeholder')) {
            el.val('');
          }
        },

        'blur': function() {
          var el = $(this);
          if (el.val() === '') {
            el.val(el.attr('placeholder'));
          }
        }
      }).each(function() {
        var el = $(this);
        if (el.val() === '') {
          el.val(el.attr('placeholder'));
        }
      });
    });
  }

  $.fn.placeholder = placeHolder;

  // -------------------------------------------------------
  // custom form
  // -------------------------------------------------------
  function customForm() {
    function customInput(input) {
      this.el = $(input);

      if (input.type === 'radio') {
        prepare(this, 'radio');
      }

      else if (input.type === 'checkbox') {
        prepare(this, 'checkbox');
      } 

      else if (input.type === 'file') {
        prepare(this, 'file');
      } 

      else if (this.el.is('select')) {
        prepare(this, 'select');
      } 

      return this;
    }

    function prepare(obj, type) {
      var tpl = {
        radio: '<label class="check radio" />',
        checkbox: '<label class="check checkbox" />',
        select: '<label class="select" />',
        file: '<label class="file" />'
      }[type];

      if (!tpl) { return }

      tpl = $(tpl);

      tpl.attr({
        'for': obj.el.attr('id')
      });

      tpl.addClass(type + '-' + obj.el.attr('name'));

      obj.el.wrap(tpl);
      obj.content = obj.el.parent();

      if (type === 'radio') {
        radioChecked(obj.content, { type: type, input: obj.el });
      }

      else if (type === 'checkbox') {
        checkboxChecked(obj.content, { type: type, input: obj.el });
      }

      else if (type === 'select') {
        obj.content.prepend('<span class="label">' +
          '<span class="text"></span></span><span class="icon"/>');
        selectCheck(obj.content, { type: type, input: obj.el });
        obj.el.width(obj.content.outerWidth());
        obj.content.find('.label').width(obj.el.width() - 35);
      }

      else if (type === 'file') {
        obj.content.prepend('<span class="label">' +
          '<span class="text">' +
          'No File Chosen' +
          '</span>' +
          '</span><span class="icon"/>');
      }

      obj.content.on('change', { type: type, input: obj.el }, interaction);
    }

    function interaction(e) {
      var el = $(e.currentTarget);

      if (e.data.type === 'radio') {
        radioChecked(el, e.data);
      }

      else if (e.data.type === 'checkbox') {
        checkboxChecked(el, e.data);
      }

      else if (e.data.type === 'select') {
        selectCheck(el, e.data);
      }

      else if (e.data.type === 'file') {
        showFileName(el, e.data);
      }
    }

    function radioChecked(el, data) {
      if (data.input.is(':checked')) {
        $('.'+ data.type + '-' + data.input.attr('name'))
          .removeClass('radio-checked');

        el.addClass('radio-checked');
      }
    }

    function checkboxChecked(el, data) {
      if (data.input.is(':checked')) {
        el.addClass('checkbox-checked');
      }
      else {
        el.removeClass('checkbox-checked');
      }
    }

    function selectCheck(el, data) {
      var value = data.input.find('option:selected').val();

      if (value === '' || value !== data.input.attr('placeholder')) {
        value = data.input.find('option[value="'+ value +'"]').html();
      }

      el.find('.text').empty().html(value);
    }

    function showFileName(el, data) {
      var value = data.input.val();
      el.find('.text').empty().html(
        value.match(/[-_\w]+[.][\w]+$/i)[0] || 'Subir archivo'
      );
    }

    this.customInput = customInput;

    return this;
  }

  // -------------------------------------------------------
  // formulario de creacion de  productos
  // -------------------------------------------------------
  function addItemForm() {
    var form = $('#form_additem');
    var inputs = {
      'title': $('#title', form),
      'category': $('#category', form),
      'description': $('#description', form),
      'price': $('#price', form),
      'duration': $('#duration', form),
      'type': $('[name="type"]', form),
      'image': $('#image', form)
    };

    form
      .validate({
        highlight: function(element, errorCls, validCls) {
          var el = $(element);
          errorCls += '-custom';
          validCls += '-custom';

          if (
            el.is('select') ||
            el.is(':file')
          ) {
            el.parent().addClass(errorCls).removeClass(validCls);
          }

          else if (el.is(':radio')) {
            $('[name="' + el.attr('name') + '"]', form).each(function() {
              $(this).parent().addClass(errorCls + '-radio').removeClass(validCls);
            });
          }

          else if (el.is(':checkbox')) {
            $('[name="' + el.attr('name') + '"]', form).each(function() {
              $(this).parent().addClass(errorCls + '-checkbox').removeClass(validCls);
            });
          }

          else {
            el.addClass(errorCls).removeClass(validCls);
          }
        },
        unhighlight: function(element, errorCls, validCls) {
          var el = $(element);
          errorCls += '-custom';
          validCls += '-custom';

          if (
            el.is('select') ||
            el.is(':file')
          ) {
            el.parent().addClass(validCls).removeClass(errorCls);
          }

          else if (el.is(':radio')) {
            $('[name="' + el.attr('name') + '"]', form).each(function() {
              $(this).parent().addClass(validCls).removeClass(errorCls + '-radio');
            });
          }

          else if (el.is(':checkbox')) {
            $('[name="' + el.attr('name') + '"]', form).each(function() {
              $(this).parent().addClass(validCls).removeClass(errorCls + '-checkbox');
            });
          }

          else {
            el.addClass(validCls).removeClass(errorCls);
          }
        },
        errorPlacement: function(error, element) {}
      });
  }

  // -------------------------------------------------------
  // App
  // -------------------------------------------------------
  function App() {
    var body = $('#body');
    var forms = new customForm();

    var dialog = {
      create: function(opt) {
        opt = opt || {};
        var content = $('<div class="custom-dialog">' + opt.content + '</div>');
        body.append(content);

        content.dialog(
            $.extend({
              width: 910,
              height: 500,
              modal: true,
              close: function() {
                content.dialog('destroy');
                content.remove();
              }
            }, opt)
        );
        return content;
      },

      acceptOr: function(opt) {
        opt = opt || {};

        opt.buttons = opt.buttons || {};

        var m = this.create({
          title: opt.title || '',
          content: opt.content || '',
          width: opt.width,
          height: opt.height,
          buttons: $.extend(opt.buttons, {
            'Cerrar': function() {
              m.dialog('close');
            }
          })
        });

        return m;
      }
    };

    window.app = {
      body: body,
      dialog: dialog,
      customForm: customForm
    };

    // show me plugin
    $('.js-showme').showMe();
    body.on('click', ShowMeEveryBody);

    // placeholder support
    if (!('placeholder' in document.createElement('input'))) {
      $('[placeholder]').placeholder();
    }

    // custom inputs
    $('.custom_form :input').each(function() {
      forms.customInput(this);
    });


    // add producto form
    addItemForm();

    // notificaciones
    $('#notifications li:last').addClass('last');
  }

  $(App);
}();
