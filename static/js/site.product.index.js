!function($) {
  function accordion() {
    var trigger = $('.accordion-trigger');
    var content = $('.accordion-content').hide();

    trigger.on('click', function(e) {
      e.preventDefault();
      var el = $(this);
      content.hide();
      el.next('.accordion-content').show();
    });
  }

 

  $(document).ready(function() {
    accordion();
  
  });

  


}(jQuery);
