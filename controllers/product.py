import settings
import logging
import datetime

from worker.tasks import generate_thumbs
from libs.utils import convert_int, convert_bool

from controllers import BaseHandler
from tornado.web import authenticated

from models import Product, Category, Points

log = logging.getLogger(__name__)

MIN_LEN_TITLE = 3
MAX_LEN_TITLE = 100

MIN_LEN_DESCRIPTION = 3
MAX_LEN_DESCRIPTION = 250

VIEW_PATH = 'site/product/%s'


class Add(BaseHandler):

    @authenticated
    def get(self, **kwargs):
        kwargs['user'] = self.get_current_user()
        kwargs['categories'] = Category.objects.all()
        self.render(VIEW_PATH % 'add.html', **kwargs)

    @authenticated
    def post(self):

        status_code = 0
        title = self.get_argument('title', None)
        description = self.get_argument('description', None)
        cost = self.get_argument('price', None)
        duration = self.get_argument('duration', None)
        category = self.get_argument('category', None)
        type = self.get_argument('type', None)
        user = self.get_current_user()

        _raw_image = self.request_file('filename')

        if settings.DEBUG:
            log.info('title: %s' % title)
            log.info('category: %s' % category)
            log.info('cost: %s' % cost)
            log.info('type: %s' % type)

        if not title or not description or not cost or \
                not duration or not category or not type or not _raw_image:
            status_code |= 1
        else:
            if not Category.get(category):
                self.send_error(404)
                return

            if len(title) < MIN_LEN_TITLE or len(title) > MAX_LEN_TITLE:
                status_code |= 2

            if len(description) < MIN_LEN_DESCRIPTION or \
                    len(description) > MAX_LEN_DESCRIPTION:
                status_code |= 4

            try:
                float(cost)
            except ValueError:
                status_code |= 6

            try:
                int(duration)
            except ValueError:
                status_code |= 8
            else:

                if not duration in dict(Product.DURATION_POST).keys():
                    status_code |= 8
                else:
                    duration = int(duration)

            if not type in dict(Product.TYPES).values():
                status_code |= 16

            if status_code == 0:

                product = Product(
                    title=title,
                    description=description,
                    cost=cost,
                    user=user.email,
                    category=category,
                    duration=duration,
                    published_at=datetime.datetime.now(),
                    type=type
                )

                try:
                    product.save()
                except Exception as exc:
                    log.error(exc)
                    status_code = 5
                else:

                    if not product.upload_img(_raw_image.get('body')):
                        status_code = 3
                        log.error('no upload image')
                    else:
                        generate_thumbs.delay(product.id)
                        product.save_totales(_categories=[category])

                        self.redirect(self.reverse_url('product_edit',
                            product.slug))
                        return

        log.info('status_code: %s' % status_code)
        self.get(status_code=status_code)


class Edit(BaseHandler):

    @authenticated
    def get(self, slug, **kwargs):
        user = self.get_current_user()
        product = Product.get_by_slug(slug)

        if not product:
            self.send_error(404)
            return

        if settings.DEBUG:
            log.info('current_user: %s' % user.email)
            log.info('product_owner: %s' % product.user)
            log.info('image: %s' % product.image)
            log.info('tyny_image: %s' % product.tiny_url)
            log.info('small_image: %s' % product.small_url)
            log.info('large_image: %s' % product.large_url)
            log.info('medium_image: %s' % product.medium_url)
            log.info('is_upload_thumbs: %s' % product.is_upload_thumbs)

        kwargs['product'] = product
        kwargs['categories'] = Category.objects.all()
        kwargs['audiences'] = Product.AUDIENCES_TYPES
        kwargs['user'] = user
        kwargs['action'] = 'edit'
        self.render(VIEW_PATH % 'add.html', **kwargs)

    @authenticated
    def post(self, slug):

        status_code = 0
        title = self.get_argument('title', None)
        category = self.get_argument('category', None)
        description = self.get_argument('description', None)
        cost = self.get_argument('price', None)
        duration = self.get_argument('duration', None)
        will_travel = convert_int(self.get_argument('will_travel', ''))
        allow_retailers = convert_bool(
            self.get_argument('allow_retailers', False))
        allow_charities = convert_bool(
            self.get_argument('allow_charities', False))
        donate_now = convert_bool(self.get_argument('donate_now', False))
        location = self.get_argument('location', '')
        audience = self.get_argument('audience', '')

        if settings.DEBUG:
            log.info('title: %s' % title)
            log.info('category: %s' % category)
            log.info('description: %s' % description)
            log.info('cost: %s' % cost)
            log.info('duration: %s' % duration)
            log.info('cost: %s' % cost)

        user = self.get_current_user()

        product = Product.get_by_slug(slug)

        if not product:
            self.send_error(404)
            return

        if not title or not description or not cost or \
                not duration or not category:
            status_code |= 1
        else:
            if not Category.get(category):
                self.send_error(404)
                return

            if len(title) < MIN_LEN_TITLE or len(title) > MAX_LEN_TITLE:
                status_code |= 2

            if len(description) < MIN_LEN_DESCRIPTION or \
                    len(description) > MAX_LEN_DESCRIPTION:
                status_code |= 4

            try:
                float(cost)
            except ValueError:
                status_code |= 6

            try:
                int(duration)
            except ValueError:
                status_code |= 8
            else:

                if not duration in dict(Product.DURATION_POST).keys():
                    status_code |= 8
                else:
                    duration = int(duration)

            if status_code == 0:

                _raw_image = self.request_file('filename')

                _status = product.status
                _type = product.type

                product.title = title
                product.description = description
                product.category = category
                product.cost = cost
                product.will_travel = will_travel
                product.allow_charities = allow_charities
                product.allow_retailers = allow_retailers
                product.donate_now = donate_now
                product.location = location
                product.audience = audience
                product.status = 'active'
                product.duration = duration

                try:
                    product.update()
                except Exception as exc:
                    log.error(exc)
                    status_code = 5
                else:

                    if _status == 'inactive':
                        points = Points(user=user.email)

                        try:
                            points.save_per_type('product')
                        except Exception as exc:
                            log.error(exc)

                        if allow_charities:

                            try:
                                points.save_per_type('allow_charities')
                            except Exception as exc:
                                log.error(exc)

                if status_code == 0:
                    if _raw_image:

                        if not product.upload_img(_raw_image.get('body')):
                            status_code = 4
                        else:
                            generate_thumbs.delay(product.id)

                    product.save_totales(
                       _categories=[category],
                       _type=_type,
                       _status=_status,
                       is_new=False
                    )

        log.info('status_code: %s' % status_code)
        self.get(slug, status_code=status_code)


class View(BaseHandler):

    def get(self, slug):

        product = Product.get_by_slug(slug)

        if not product:
            self.send_error(404)
            return

        tot_unread_comments = 0
        conversations = []
        comments = []
        is_owner = False
        user = self.get_current_user()

        if user:
            is_owner = product.is_owner(user)

            log.info('is_owner: %s' % is_owner)
            log.info('current_user: %s' % user.email)
            log.info('product_owner: %s' % product.user)

            if not is_owner:
                conversation = product.get_conversation(user.email)

                if conversation:
                    comments = [dict(user=x.user_data.name,
                        text=x.description,
                        created_at=x.created_at_f) \
                            for x in conversation.comments]
            else:

                for c in product.conversations:

                    _comments = [dict(user=x.user_data.name,
                        is_owner=1 if x.user == product.user else 0,
                        text=x.description,
                        created_at=x.created_at_f) \
                            for x in c.comments]

                    tot_unread_comments += int(c.unread_comments)
                    conversations.append(dict(id=c.id,
                        user=c.user_data.name,
                        unread_comments=c.unread_comments,
                        comments=_comments))

        self.render(VIEW_PATH % 'view.html',
            user=user,
            product=product,
            is_owner=is_owner,
            comments=comments,
            conversations=conversations,
            tot_unread_comments=tot_unread_comments)


class Delete(BaseHandler):

    @authenticated
    def post(self, id):

        if self.is_header_xml_http_request():
            self.send_error(403)
            return

        product = Product.get(id)

        if not product:
            self.send_error(403)
            return

        status_code = 0

        try:
            product.delete()
        except Exception as exc:
            log.error(exc)
            status_code = 1

        self.finish({'status_code': status_code})
