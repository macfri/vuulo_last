import logging

from controllers import BaseHandler
from tornado.web import authenticated

from models import Product, Match, Points

log = logging.getLogger(__name__)


class Add(BaseHandler):

    @authenticated
    def get(self):

        status_code = 0
        #source = want
        #target = have
        user = self.get_current_user()

        product_1 = self.get_argument('product_1', None)
        product_2 = self.get_argument('product_2', None)
        message = self.get_argument('message', None)

        log.info('message: %s' % message)

        wait = self.get_argument('wait', None)

        if product_1 == product_2:
            status_code = 1
            log.info('same product!')
        else:

            product_1 = Product.get(product_1)

            if not product_1:
                self.send_error(404)
                return

            if wait:
                if product_1.user == user.email:
                    log.info('same user with wait')
                    status_code = 5

            if status_code == 0:

                product_2 = Product.get(product_2)

                if not product_2:
                    self.send_error(404)
                    return

                match = Match.objects.filter(
                    source=product_1.id,
                    target=product_2.id,
                    user=user.email
                )

                if len(match) < 1:
                    match = Match()
                    match.source = product_1.id
                    match.target = product_2.id
                    match.user = user.email

                    if wait:
                        match.status = 'inactive'

                    try:
                        match.save()
                    except Exception as exc:
                        log.error(exc)
                        status_code = 2
                    else:

                        if not wait:
                            points = Points(user=user.email)

                            try:
                                points.save_per_type('matching')
                            except Exception as exc:
                                log.error(exc)
                else:
                    status_code = 3

        log.info('status_code: %s' % status_code)
        self.finish(dict(status_code=status_code))


class Active(BaseHandler):

    @authenticated
    def post(self):

        status_code = 0
        id = self.get_argument('id', None)

        if not id:
            status_code = 1
            log.error('id: %s' % id)
        else:
            try:
                match = Match.objects.get(id)
            except Exception as exc:
                log.error(exc)
                status_code = 1
            else:
                match.status = 'active'
                match.type = 'match_have'

                try:
                    match.update()
                except Exception as exc:
                    log.error(exc)
                    status_code = 1

        log.info('status_code: %s' % status_code)
        self.finish(dict(status_code=status_code))
