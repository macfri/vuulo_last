import logging
import settings

from tornado.auth import FacebookGraphMixin
from models import User
from controllers import BaseHandler
from tornado.web import authenticated, asynchronous
from worker.tasks import send_email
from libs.utils import check_email

from datetime import datetime

log = logging.getLogger(__name__)


class Add(BaseHandler):

    def get(self, **kwargs):
        self.render('site/user/add.html', **kwargs)

    def post(self):

        status_code = 0

        name = self.get_argument('name', '')
        lastname = self.get_argument('lastname', '')
        gender = self.get_argument('gender', None)
        password = self.get_argument('password', '')
        confirm_password = self.get_argument('confirm_password', '')
        email = self.get_argument('email', '')

        if len(name) < 1 or len(lastname) < 1 or \
            len(password) < 1 or len(email) < 1:
            status_code = 1
        elif not (password == confirm_password):
            status_code = 2
        elif not check_email(email):
            status_code = 3
        elif not gender in ('male', 'female'):
            status_code = 4
        elif User.email_exists(email):
            status_code = 5

        if status_code == 0:
            user = User()
            user.email = email
            user.password = password
            user.name = name
            user.lastname = lastname
            user.gender = gender

            try:
                user.save()
            except Exception as exc:
                log.error(exc)
                status_code = 6
            else:
                content = self.render_string(
                    "mail/confirmation.html",
                    first_name=name,
                    last_name=lastname,
                    url=self.reverse_url("user_activate",
                        user.activation),
                    settings=self.settings
                )

                mail = dict(
                    sender=settings.AWS_EMAIL_FROM,
                    subject='Registration confirmation',
                    body=content,
                    to=['m4cf1.server@gmail.com']
                )

                send_email.delay(mail)
                status_code = 7

        self.get(status_code=status_code)


class Edit(BaseHandler):

    @authenticated
    def get(self, **kwargs):

        user = self.get_current_user()
        log.info('photo: %s' % user.photo)

        self.render('site/user/add.html',
            user=user,
            **kwargs
        )

    @authenticated
    def post(self):
        status_code = 0
        name = self.get_argument('name', '')
        lastname = self.get_argument('lastname', '')
        gender = self.get_argument('gender', None)
        password = self.get_argument('password', '')
        confirm_password = self.get_argument('confirm_password', '')
        user = self.get_current_user()

        print self.request.files

        try:
            _raw_image = self.request.files['filename'][0]
        except (KeyError, IndexError) as exc:
            _raw_image = None

        if len(name) < 1 or len(lastname) < 1:
            status_code = 1
        elif len(password) > 0:
            if not (password == confirm_password):
                status_code = 2
        elif gender not in ('male', 'female'):
            status_code = 4

        if status_code == 0:

            user.name = name
            user.lastname = lastname
            user.gender = gender

            if password:
                user.set_password(password)

            try:
                user.update()
            except Exception as exc:
                log.error(exc)
                status_code = 6
            else:

                if _raw_image:

                    if not user.upload_img(_raw_image.get('body')):
                        log.error('no upload')
                    else:
                        log.info('upload photo')

                status_code = 8

        self.get(status_code=status_code)


class Activate(BaseHandler):
    def get(self, token):

        user = User.get_by_activation(token)

        if not user:
            self.send_error(404)
            return

        user.status = 'active'
        user.activation = ''

        try:
            user.update()
        except Exception as exc:
            log.error(exc)
            self.send_error(409)
            return

        self.set_secure_cookie('user', user.email)
        self.redirect(self.reverse_url("home"))
        return


class SetPasswordFacebook(BaseHandler):

    def get(self, token, **kwargs):
        status_code = None

        if 'status_code' in kwargs:
            status_code = kwargs['status_code']

        user = User.get_by_activation_passwd_facebook(token)

        if not user:
            self.send_error(404)
            return

        self.render('site/user/facebook/set_password.html',
            status_code=status_code)

    def post(self, token):
        status_code = 0

        user = User.get_by_activation_passwd_facebook(token)

        if not user:
            self.send_error(404)
            return

        password = self.get_argument('password', '')

        if len(password) < 1:
            status_code = 1
        else:
            user.set_password(password)

            try:
                user.update()
            except Exception as exc:
                status_code = 2
                log.error(exc)
            else:

                user.activation_passwd_facebook = ''

                try:
                    user.update()
                except Exception as exc:
                    status_code = 2
                    log.error(exc)

                self.set_secure_cookie('user', user.email)
                self.redirect(self.reverse_url("home"))
                return

        self.get(token, status_code=status_code)


class Login(BaseHandler):

    def get(self, **kwargs):
        self.render('site/user/login.html', **kwargs)

    def post(self):

        status_code = 0
        email = self.get_argument('email', '')
        password = self.get_argument('password', '')

        if len(email) < 5 or len(password) < 1:
            status_code = 1
        elif not check_email(email):
            status_code = 1
        else:
            if User.auth(email, password):
                user = User.get(email)

                if user:
                    user.last_login = datetime.now()

                try:
                    user.update()
                except Exception as exc:
                    log.error(exc)

                self.set_secure_cookie('user', email)
            else:
                status_code = 2
        log.info('status_code: %s' % status_code)
        self.finish(dict(status_code=status_code))


class LoginFacebook(FacebookGraphMixin, BaseHandler):

    @asynchronous
    def get(self):
        url = "%s://%s%s" % (
            self.request.protocol,
            self.request.host,
            self.reverse_url("user_login_facebook")
        )

        if self.get_argument('code', None):
            self.get_authenticated_user(
                redirect_uri=url,
                client_id=self.settings['facebook_api_key'],
                client_secret=self.settings['facebook_secret'],
                code=self.get_argument('code'),
                callback=self._on_auth,
            )
            return

        self.authorize_redirect(
            redirect_uri=url,
            client_id=self.settings['facebook_api_key'],
            extra_params={"scope": "email"}
        )

    def _on_auth(self, user):
        if not user:
            self.send_error(500)
        else:
            self.facebook_request(
                '/me',
                self._on_stream,
                access_token=user['access_token']
            )
        return

    def _on_stream(self, data):
        if not data:
            self.send_error(500)
            return

        save_ok = True
        email = data['email']

        if not User.email_exists(email):
            log.info('save fb_user')
            user = User()
            user.email = email
            user.name = data['first_name']
            user.lastname = '%s %s' % (
                data['middle_name'], data['last_name'])
            user.gender = data['gender']
            user.facebook_id = data['id']
            user.status = 'active'

            try:
                user.save()
            except Exception as exc:
                save_ok = False
                log.error(exc)
            else:

                content = self.render_string(
                    "mail/set_passwd_facebook.html",
                    first_name=user.name,
                    last_name=user.lastname,
                    url=self.reverse_url("user_set_password_facebook",
                        user.activation_passwd_facebook),
                    settings=self.settings
                )

                mail = dict(sender=settings.AWS_EMAIL_FROM,
                    subject='Set Password',
                    body=content,
                    to=['m4cf1.server@gmail.com']
                )

                send_email.delay(mail)
                self.redirect(self.reverse_url('confirmation'))

        else:
            log.info('exits email')
            user = User.objects.get(email)

            if not user.facebook_id:
                log.info('exists email but not fbid')
                user.facebook_id = data.get('id')

                try:
                    user.update()
                except Exception as exc:
                    save_ok = False
                    log.error(exc)

        if save_ok:
            self.set_secure_cookie('user', email)
            self.redirect(self.reverse_url('home'))
        else:
            self.render('site/user/login.html', status_code=2)
        return


class Logout(BaseHandler):

    def get(self):
        self.clear_cookie("user")
        self.redirect(self.reverse_url('home'))


class ForgotPassword(BaseHandler):

    def get(self, **kwargs):
        self.render('site/user/forgot_password.html', **kwargs)

    def post(self):
        status_code = 0
        email = self.get_argument('email', '')

        if len(email) < 4 or not check_email(email):
            status_code = 1
        else:
            user = User.get(email)

            if not user:
                status_code = 2
            else:
                user.activation_passwd = User.generate_token(
                    'activation_passwd')

                try:
                    user.update()
                except Exception as exc:
                    log.error(exc)
                else:
                    content = self.render_string(
                        "mail/change_password.html",
                        first_name=user.name,
                        last_name=user.lastname,
                        url=self.reverse_url("user_change_password",
                                user.activation_passwd),
                        settings=self.settings
                    )

                    mail = dict(sender=settings.AWS_EMAIL_FROM,
                        subject='Change password',
                        body=content,
                        to=['m4cf1.server@gmail.com']
                    )

                    send_email.delay(mail)
                    status_code = 3

        self.get(status_code=status_code)


class ChangePassword(BaseHandler):

    def get(self, token, **kwargs):
        user = User.get_by_activation_passwd(token)

        if not user:
            self.send_error(405)
            return
        else:
            self.set_cookie('token_pwd', user.activation_passwd)

        self.render('site/user/change_password.html', **kwargs)

    def post(self, token, **kwargs):

        status_code = 0

        password = self.get_argument('password', '')
        confirm_password = self.get_argument('confirm_password', '')

        user = User.get_by_activation_passwd(token)

        if not user:
            self.send_error(405)
            return

        if len(password) < 4 or len(confirm_password) < 4:
            status_code = 1
        elif not (password == confirm_password):
            status_code = 2

        if status_code == 0:
            user.set_password(password)

            try:
                user.update()
            except Exception as exc:
                log.error(exc)
                status_code = 3
            else:
                self.clear_cookie('token_pwd')
                status_code = 4

        self.get(token, status_code=status_code)


class HaveProducts(BaseHandler):

    @authenticated
    def get(self):

        #products = Product.objects.filter(type='have', status='active')

        #page = self.get_argument('page', 1)
        #limit = self.get_argument('limit', 10)

        """
        try:
            page = int(page)
        except Exception as exc:
            log.error(exc)
            page = 1
        else:
            if page < 1:
                page = 1

        try:
            limit = int(limit)
        except Exception as exc:
            log.error(exc)
            limit = 10
        """

        user = self.get_current_user()
        #products = products.filter(user=user.email)

        #offset = (page - 1) * limit
        #limit = page * limit

        self.render('site/user/have_products.html',
            products=user.have_products)


class Confirmation(BaseHandler):

    def get(self):
        self.render('site/user/facebook/confirmation.html')
