import logging

from tornado.web import HTTPError, authenticated

from controllers import BaseHandler, ListMixin
from models import Product, Category, Match

from boto.s3.connection import S3Connection

import datetime
import settings

#from forms import CategoryForm
from forms import ProductForm


class List(BaseHandler, ListMixin):

    #@authenticated
    def get(self):


        filters = {'created_at__noteq': None}
        status = self.get_argument('status', None)

        if status:
            if 'active' in status or 'inactive' in status:
                filters['status'] = status

        queryset = Product.objects.filter(**filters)
        total = len(queryset)

        self.render(
            'admin/product/list.html',
            #'admin/product/list.jade',
            status=status,
            **self.get_pagination(count=total, query=queryset, per_page=50)
        )

    def post(self):

        delete_items = self.get_arguments('delete_items', None)

        if delete_items:

            for x in delete_items:

                product = Product.get(x)

                if product:

                    try:
                        product.delete()
                    except Exception as exc:
                        logging.error(exc)
                        pass
                    else:
                        print "delete item"

        self.get()


class Add(BaseHandler):

    def get(self, **kwargs):

        #form = CategoryForm()
        form = ProductForm()
        kwargs['form'] = form
        self.render('admin/product/add.html', **kwargs)

    def post(self):

        forms_data = MultiDict(self.request.arguments)
        logging.info(forms_data)

        form = CategoryForm(self.request_data)

        if form.validate():

            logging.info(form.title.data)

            product = Category(
                title=form.title.data,
                status=form.status.data
            )

            try:
                product.save()
            except Exception as exc:
                logging.error(exc)
            else:
                self.redirect(self.reverse_url('admin_product_edit',
                    product.slug))
                return
        else:

            logging.error(form.errors)

        self.get()


class Edit(BaseHandler):

    @authenticated
    def get(self, id, **kwargs):
        user = self.get_current_user()

        try:
            product = Product.objects.get(id)
        except:
            self.send_error(404)
            return

        if not product.title:
            product.title = 'product'

        if not product.description:
            product.description = 'This is a brief description..'

        if not product.cost:
            product.cost = 50

        if not product.will_travel:
            product.will_travel = 1000

        if product.published_at:
            try:
                product.published_at = product.published_at.date()
            except Exception as exc:
                logging.error(exc)
        else:
            product.published_at = datetime.datetime.now().strftime('%Y-%m-%d')

        if product.expires_at:
            try:
                product.expires_at = product.expires_at.date()
            except Exception as exc:
                logging.error(exc)
        else:
            product.expires_at = (datetime.datetime.now() + \
                datetime.timedelta(days=5)).strftime('%Y-%m-%d')

        if not product.location:
            product.location = 'WC 12 avenue'

        categories = Category.objects.all()

        """
        if not product.categories:
            product.categories = [u'metarials', u'sporting']
        """

        """
        if product.categories:
            for x, y in enumerate(categories):
                if y.title.lower() in product.categories:
                    categories[x].checked = True
                else:
                    categories[x].checked = False
        """

        logging.info('image: %s' % product.image)
        logging.info('tyny_image: %s' % product.tiny_url)
        logging.info('small_image: %s' % product.small_url)
        logging.info('large_image: %s' % product.large_url)
        logging.info('medium_image: %s' % product.medium_url)
        logging.info('is_upload_thumbs: %s' % product.is_upload_thumbs)

        kwargs['product'] = product
        kwargs['categories'] = categories
        kwargs['audiences'] = Product.AUDIENCES_TYPES
        kwargs['user'] = user
        kwargs['action'] = 'edit'
        self.render('product.html', **kwargs)

    @authenticated
    def post(self, id):
        status_code = 0

        categories = self.get_arguments('categories', None)
        title = self.get_argument('title', '')
        description = self.get_argument('description', '')
        cost = self.get_argument('cost', '')
        will_travel = self.get_argument('will_travel', '')
        allow_retailers = self.get_argument('allow_retailers', None)
        allow_charities = self.get_argument('allow_charities', None)
        donate_now = self.get_argument('donate_now', None)
        published_at = self.get_argument('published_at', '')
        expires_at = self.get_argument('expires_at', '')
        type = self.get_argument('type', None)
        location = self.get_argument('location', '')
        audience = self.get_argument('audience', '')

        try:
            will_travel = int(will_travel)
        except Exception as exc:
            logging.error(exc)
            will_travel = 0

        try:
            cost = int(cost)
        except Exception as exc:
            logging.error(exc)
            cost = 0

        user = self.get_current_user()

        if not user:
            self.send_error(404)

        try:
            product = Product.objects.get(id)
        except Exception as exc:
            self.send_error(404)

        if len(title) < 1 or len(description) < 1 or len(published_at) < 1:
            status_code = 1
        elif not type in ('have', 'want'):
            status_code = 1

        if status_code == 0:
            try:
                published_at = datetime.datetime.strptime(
                    published_at, '%Y-%m-%d')
            except ValueError as exc:
                logging.error(exc)
                status_code = 2

            logging.info(expires_at)

            try:
                expires_at = datetime.datetime.strptime(
                    expires_at, '%Y-%m-%d')
            except ValueError as exc:
                logging.error(exc)
                status_code = 2

        if status_code == 0:
            if not categories:
                status_code = 4

        if status_code == 0:
            try:
                _raw_image = self.request.files['filename'][0]
            except (KeyError, IndexError) as exc:
                _raw_image = None

            product.title = title
            product.description = description
            product.cost = cost
            product.will_travel = will_travel
            product.allow_charities = True if allow_charities else False
            product.allow_retailers = True if allow_retailers else False
            product.donate_now = True if donate_now else False
            product.published_at = published_at
            product.expires_at = expires_at
            product.type = type
            product.user = user.email
            product.categories = categories
            product.location = location
            product.audience = audience
            product.status = 'active'
            product.set_slug(title)

            try:
                product.update()
            except Exception as exc:
                logging.error(exc)
                status_code = 5

            if status_code == 0:
                if _raw_image:

                    if not product.upload_img(_raw_image.get('body')):
                        status_code = 4
                    else:
                        if not product.send_message_to_generate_thumbs():
                        #if not product.generate_thumbs():
                            logging.error('no generate slug')
                            #status_code = 4
                        #generate_thumbs.delay(product.id)

                    if status_code == 0:
                        self.redirect(self.reverse_url('product_edit',
                            product.id))
                        return

        self.get(id, status_code=status_code)


class Delete(BaseHandler):

    #@authenticated
    #def post(self):
    def get(self):

        #if self.request.headers.get('X-Requested-With') != 'XMLHttpRequest':
            #raise HTTPError(403)

        status_code = 0

        try:
            product = Product.objects.get(self.get_argument('id'))
        except Exception as exc:
            status_code = 1
            logging.error(exc)
            raise HTTPError(404)
        else:

            """
            for conversation in product.conversations:
                print conversation
                conversation.delete()

            for match in Match.objects.filter(source=product.id):
                print match
                match.delete()

            for match in Match.objects.filter(target=product.id):
                print match
                match.delete()
            """

            try:
                product.delete()
            except Exception as exc:
                logging.error(exc)
                status_code = 2
            else:
                cn = S3Connection(
                    settings.AWS_KEY, settings.AWS_SECRET
                )

                for suffix in ('', '_large', '_medium', '_small', '_tiny'):

                    print '%s/%s%s' % (settings.S3_UPLOAD_PATH_PRODUCT,
                        product.id, suffix)

                    try:
                        key = cn.get_bucket(settings.S3_BUCKET_NAME).get_key(
                            '%s/%s%s' % (settings.S3_UPLOAD_PATH_PRODUCT,
                                product.id, suffix))
                        key.delete()
                    except Exception as exc:
                        logging.error(exc)

        self.finish({'status_code': status_code})


class DeleteImage(BaseHandler):

    #@authenticated
    def post(self):
        #if self.request.headers.get('X-Requested-With') != 'XMLHttpRequest':
            #raise HTTPError(403)

        status_code = 0

        try:
            product = Product.objects.get(self.get_argument('id'))
        except Exception as exc:
            logging.error(exc)
        else:

            cn = S3Connection(
                settings.AWS_KEY, settings.AWS_SECRET
            )

            key = cn.get_bucket(settings.S3_BUCKET_NAME).get_key(
                '%s/%s' % (settings.S3_UPLOAD_PATH_PRODUCT, product.id))
            key.delete()

        self.finish({'status_code': status_code})
