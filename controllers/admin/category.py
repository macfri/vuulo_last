import logging
import settings

from tornado.web import HTTPError, authenticated
from controllers import BaseHandler, ListMixin
from models import Category
from forms import CategoryForm
from boto.s3.connection import S3Connection
from paste.util.multidict import MultiDict


class List(BaseHandler, ListMixin):

    def get(self):

        filters = {'created_at__noteq': None}
        status = self.get_argument('status', None)

        if status:
            print "ooo"
            if 'active' in status or 'inactive' in status:
                filters['status'] = status

        queryset = Category.objects.filter(**filters)

        for x in queryset:
            print x.title, x.total_products_want, x.total_products_have


        total = len(queryset)

        self.render(
            'admin/category/list.html',
            #'admin/category/list.jade',
            status=status,
            **self.get_pagination(count=total, query=queryset, per_page=50)
        )


class Add(BaseHandler):

    def get(self, **kwargs):

        form = CategoryForm()
        kwargs['form'] = form
        self.render('admin/category/add.html', **kwargs)

    def post(self):

        forms_data = MultiDict(self.request.arguments)
        logging.info(forms_data)

        form = CategoryForm(self.request_data)

        if form.validate():

            logging.info(form.title.data)

            category = Category(
                title=form.title.data,
                status=form.status.data
            )

            try:
                category.save()
            except Exception as exc:
                logging.error(exc)
            else:
                self.redirect(self.reverse_url('admin_category_edit',
                    category.slug))
                return
        else:

            logging.error(form.errors)

        self.get()


class Edit(BaseHandler):

    def get(self, slug, **kwargs):

        try:
            category = Category.objects.get(slug)
        except:
            self.send_error(404)
            return

        form = CategoryForm(obj=category)

        kwargs['form'] = form
        self.render('admin/category/add.html', **kwargs)

    def post(self, slug):

        forms_data = MultiDict(self.request.arguments)
        logging.info(forms_data)

        form = CategoryForm(self.request_data)

        try:
            category = Category.objects.get(slug)
        except:
            self.send_error(404)
            return

        if form.validate():

            category.title = form.title.data
            category.status = form.status.data

            try:
                category.update()
            except Exception as exc:
                logging.error(exc)

        else:

            logging.error(form.errors)

        self.get(slug)


class Delete(BaseHandler):

    def get(self):

        status_code = 0

        try:
            category = Category.objects.get(self.get_argument('id'))
        except Exception as exc:
            status_code = 1
            logging.error(exc)
            raise HTTPError(404)
        else:

            try:
                category.delete()
            except Exception as exc:
                logging.error(exc)
                status_code = 2

        self.finish({'status_code': status_code})


class DeleteImage(BaseHandler):

    def post(self):

        status_code = 0

        try:
            category = Category.objects.get(self.get_argument('id'))
        except Exception as exc:
            logging.error(exc)
        else:

            cn = S3Connection(
                settings.AWS_KEY, settings.AWS_SECRET
            )

            key = cn.get_bucket(settings.S3_BUCKET_NAME).get_key(
                '%s/%s' % (settings.S3_UPLOAD_PATH_PRODUCT, category.id))
            key.delete()

        self.finish({'status_code': status_code})
