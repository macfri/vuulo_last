import logging

from datetime import datetime
from controllers import BaseHandler
from models import SystemUser


class Login(BaseHandler):

    def get(self, status_code=None):

        if self.current_user:
            self.redirect(self.reverse_url('admin_product'))
            return

        self.render('admin/login.html', status_code=status_code)

    def post(self):
        username = self.get_argument('username')
        password = self.get_argument('password')
        status_code = 0

        if not username or not password:
            status_code = 1
        else:
            _user = SystemUser.auth(username, password)
            if _user:
                _user.last_login_at = datetime.now()
                try:
                    _user.save()
                except Exception as exc:
                    logging.error(exc)
                    status_code = 3
                else:
                    self.set_secure_cookie('user', str(_user.id))
                    self.redirect(
                        self.get_argument(
                            'next',
                            self.reverse_url('admin_product')
                        )
                    )
                return
            else:
                status_code = 2

        self.get(status_code)


class Logout(BaseHandler):

    def get(self):
        if self.current_user:
            self.clear_all_cookies()
        self.redirect(self.reverse_url('admin_login'))
