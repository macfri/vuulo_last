import logging

from tornado.web import HTTPError, authenticated

from controllers import BaseHandler, ListMixin
from models import User

from boto.s3.connection import S3Connection

import datetime
import settings


class List(BaseHandler, ListMixin):

    #@authenticated
    def get(self):

        filters = {'created_at__gte': datetime.date(2012, 01, 01)}
        status = self.get_argument('status', None)

        if status:
            filters['status'] = status

        #queryset = User.objects.order_by('-id')
        queryset = User.objects.filter(**filters).order_by('-created_at')

        #created_at__gte=datetime.date(2012, 01, 01)
        #.order_by('-created_at')

        self.render(
            'admin/user/list.html',
            status=status,
            **self.get_pagination(count=len(queryset), query=queryset)
        )


class Edit(BaseHandler):

    @authenticated
    def get(self, id, **kwargs):
        user = self.get_current_user()

        try:
            user = User.objects.get(id)
        except:
            self.send_error(404)
            return

        if not user.title:
            user.title = 'user'

        if not user.description:
            user.description = 'This is a brief description..'

        if not user.cost:
            user.cost = 50

        if not user.will_travel:
            user.will_travel = 1000

        if user.published_at:
            try:
                user.published_at = user.published_at.date()
            except Exception as exc:
                logging.error(exc)
        else:
            user.published_at = datetime.datetime.now().strftime('%Y-%m-%d')

        if user.expires_at:
            try:
                user.expires_at = user.expires_at.date()
            except Exception as exc:
                logging.error(exc)
        else:
            user.expires_at = (datetime.datetime.now() + \
                datetime.timedelta(days=5)).strftime('%Y-%m-%d')

        if not user.location:
            user.location = 'WC 12 avenue'

        categories = Category.objects.all()

        if not user.categories:
            user.categories = [u'metarials', u'sporting']

        if user.categories:
            for x, y in enumerate(categories):
                if y.title.lower() in user.categories:
                    categories[x].checked = True
                else:
                    categories[x].checked = False

        logging.info('image: %s' % user.image)
        logging.info('tyny_image: %s' % user.tiny_url)
        logging.info('small_image: %s' % user.small_url)
        logging.info('large_image: %s' % user.large_url)
        logging.info('medium_image: %s' % user.medium_url)
        logging.info('is_upload_thumbs: %s' % user.is_upload_thumbs)

        kwargs['user'] = user
        kwargs['categories'] = categories
        kwargs['audiences'] = User.AUDIENCES_TYPES
        kwargs['user'] = user
        kwargs['action'] = 'edit'
        self.render('user.html', **kwargs)

    @authenticated
    def post(self, id):
        status_code = 0

        categories = self.get_arguments('categories', None)
        title = self.get_argument('title', '')
        description = self.get_argument('description', '')
        cost = self.get_argument('cost', '')
        will_travel = self.get_argument('will_travel', '')
        allow_retailers = self.get_argument('allow_retailers', None)
        allow_charities = self.get_argument('allow_charities', None)
        donate_now = self.get_argument('donate_now', None)
        published_at = self.get_argument('published_at', '')
        expires_at = self.get_argument('expires_at', '')
        type = self.get_argument('type', None)
        location = self.get_argument('location', '')
        audience = self.get_argument('audience', '')

        try:
            will_travel = int(will_travel)
        except Exception as exc:
            logging.error(exc)
            will_travel = 0

        try:
            cost = int(cost)
        except Exception as exc:
            logging.error(exc)
            cost = 0

        user = self.get_current_user()

        if not user:
            self.send_error(404)

        try:
            user = User.objects.get(id)
        except Exception as exc:
            self.send_error(404)

        if len(title) < 1 or len(description) < 1 or len(published_at) < 1:
            status_code = 1
        elif not type in ('have', 'want'):
            status_code = 1

        if status_code == 0:
            try:
                published_at = datetime.datetime.strptime(
                    published_at, '%Y-%m-%d')
            except ValueError as exc:
                logging.error(exc)
                status_code = 2

            logging.info(expires_at)

            try:
                expires_at = datetime.datetime.strptime(
                    expires_at, '%Y-%m-%d')
            except ValueError as exc:
                logging.error(exc)
                status_code = 2

        if status_code == 0:
            if not categories:
                status_code = 4

        if status_code == 0:
            try:
                _raw_image = self.request.files['filename'][0]
            except (KeyError, IndexError) as exc:
                _raw_image = None

            user.title = title
            user.description = description
            user.cost = cost
            user.will_travel = will_travel
            user.allow_charities = True if allow_charities else False
            user.allow_retailers = True if allow_retailers else False
            user.donate_now = True if donate_now else False
            user.published_at = published_at
            user.expires_at = expires_at
            user.type = type
            user.user = user.email
            user.categories = categories
            user.location = location
            user.audience = audience
            user.status = 'active'
            user.set_slug(title)

            try:
                user.update()
            except Exception as exc:
                logging.error(exc)
                status_code = 5

            if status_code == 0:
                if _raw_image:

                    if not user.upload_img(_raw_image.get('body')):
                        status_code = 4
                    else:
                        if not user.send_message_to_generate_thumbs():
                        #if not user.generate_thumbs():
                            logging.error('no generate slug')
                            #status_code = 4
                        #generate_thumbs.delay(user.id)

                    if status_code == 0:
                        self.redirect(self.reverse_url('user_edit',
                            user.id))
                        return

        self.get(id, status_code=status_code)


class Delete(BaseHandler):

    #@authenticated
    #def post(self):
    def get(self):

        #if self.request.headers.get('X-Requested-With') != 'XMLHttpRequest':
            #raise HTTPError(403)

        status_code = 0

        try:
            user = User.objects.get(self.get_argument('id'))
        except Exception as exc:
            status_code = 1
            logging.error(exc)
            raise HTTPError(404)
        else:

            for conversation in user.conversations:
                print conversation
                conversation.delete()

            for match in Match.objects.filter(source=user.id):
                print match
                match.delete()

            for match in Match.objects.filter(target=user.id):
                print match
                match.delete()

            try:
                user.delete()
            except Exception as exc:
                logging.error(exc)
                status_code = 2
            else:
                cn = S3Connection(
                    settings.AWS_KEY, settings.AWS_SECRET
                )

                for suffix in ('', '_large', '_medium', '_small', '_tiny'):

                    print '%s/%s%s' % (settings.S3_UPLOAD_PATH_PRODUCT,
                        user.id, suffix)

                    try:
                        key = cn.get_bucket(settings.S3_BUCKET_NAME).get_key(
                            '%s/%s%s' % (settings.S3_UPLOAD_PATH_PRODUCT,
                                user.id, suffix))
                        key.delete()
                    except Exception as exc:
                        logging.error(exc)

        self.finish({'status_code': status_code})


class DeleteImage(BaseHandler):

    #@authenticated
    def post(self):
        #if self.request.headers.get('X-Requested-With') != 'XMLHttpRequest':
            #raise HTTPError(403)

        status_code = 0

        try:
            user = User.objects.get(self.get_argument('id'))
        except Exception as exc:
            logging.error(exc)
        else:

            cn = S3Connection(
                settings.AWS_KEY, settings.AWS_SECRET
            )

            key = cn.get_bucket(settings.S3_BUCKET_NAME).get_key(
                '%s/%s' % (settings.S3_UPLOAD_PATH_PRODUCT, user.id))
            key.delete()

        self.finish({'status_code': status_code})

