import settings
import logging

from tornado.web import authenticated

from worker.tasks import send_email
from controllers import BaseHandler
from controllers.product import View
from models import Product, Conversation, Comment

log = logging.getLogger(__name__)


class SendMessageToOwner(View):

    @authenticated
    def post(self, slug):

        product = Product.get_by_slug(slug)

        type = self.get_argument('type', None)

        if not type in Conversation.TYPES:
            self.send_error(404)
            return

        if not product:
            self.send_error(404)
            return

        status_code = 0
        message = self.get_argument('message', '')

        if len(message) < 3:
            status_code = 1
        else:

            user = self.get_current_user()

            if product.is_owner(user):
                log.error('same user')
                self.send_error(404)
                return

            try:
                conversation = Conversation.objects.filter(
                    product=product.id,
                    with_user=user.email,
                    type=type
                )
            except Exception as exc:
                log.error(exc)
            else:
                try:
                    conversation = conversation[0]
                except:
                    pass

            if not conversation:
                conversation = Conversation()
                conversation.product = product.id
                conversation.with_user = user.email
                conversation.type = 'private'

                try:
                    conversation.save()
                except Exception as exc:
                    log.error(exc)
                    status_code = 2

            comment = Comment()
            comment.description = message
            comment.conversation = conversation.id
            comment.user = user.email

            try:
                comment.save()
            except Exception as exc:
                log.error(exc)
                status_code = 2
            else:

                content = self.render_string(
                    "mail/conversation.html",
                    name=product.user,
                    message=message
                )

                if product.data_user.email_notification:

                    mail = dict(
                        sender=settings.AWS_EMAIL_FROM,
                        subject='VUULO - New Message',
                        body=content,
                        to=['m4cf1.server@gmail.com']
                    )

                    send_email.delay(mail)

                conversation.increase_unread_comments()

                try:
                    conversation.update()
                except Exception as exc:
                    log.error('b')
                    log.error(exc)
                    status_code = 2

        log.info('status_code: %s' % status_code)
        self.finish(dict(status_code=status_code,
            conversation_id=conversation.id))
        return


class SendMessageFromOwner(View):

    @authenticated
    def post(self, slug):
        product = Product.get_by_slug(slug)

        if not product:
            self.send_error(404)
            return

        status_code = 0
        conversation_id = self.get_argument('conversation_id', None)

        message = self.get_argument('message', '')

        if len(message) < 3 or not conversation_id:
            status_code = 1
        else:

            user = self.get_current_user()

            if not product.is_owner(user):
                self.send_error(404)
                return

            conversation = Conversation.get(conversation_id)

            if not conversation:
                self.send_error(404)
                return
            else:
                comment = Comment()
                comment.description = message
                comment.conversation = conversation.id
                comment.user = user.email

                try:
                    comment.save()
                except Exception as exc:
                    log.error(exc)
                    status_code = 2
                else:

                    if conversation.user_data.email_notification:

                        content = self.render_string(
                            "mail/conversation.html",
                            name=conversation.user_data.name,
                            message=message
                        )

                        mail = dict(
                            sender=settings.AWS_EMAIL_FROM,
                            subject='VUULO - New Message',
                            body=content,
                            to=['m4cf1.server@gmail.com']
                        )

                        send_email.delay(mail)

        log.info('status_code: %s' % status_code)
        self.finish(dict(status_code=status_code))
        return


class ResetUnreadComments(BaseHandler):

    def post(self):

        status_code = 0
        conversation_id = self.get_argument('conversation_id', None)

        if not conversation_id:
            status_code = 1
            log.error('conversation_id: %s' % conversation_id)
        else:

            conversation = Conversation.get(conversation_id)

            if not conversation:
                status_code = 1
                log.error('no conversation object')
            else:
                conversation.unread_comments = 0

                try:
                    conversation.update()
                except Exception as exc:
                    log.error(exc)
                    status_code = 1

        self.finish(dict(status_code=status_code))
