import logging

from models import User, Points, Product, Conversation
from tornado.web import RequestHandler

from libs.pagination import Paginator
from libs.utils import DummyDict


class BaseHandler(RequestHandler):

    def __init__(self, *args, **kwargs):
        super(BaseHandler, self).__init__(*args, **kwargs)

    def get_current_user(self):
        _user = self.get_secure_cookie("user")
        if _user:
            try:
                _user = User.objects.get(_user, consistent_read=True)
            except Exception as exc:
                logging.error(exc)
            else:
                return _user
        return None

    def render_string(self, template, **kwargs):
        kwargs.update({'handler': self})
        return self.settings.get('template_env')\
            .get_template(template).render(**kwargs)

    def render(self, template, **kwargs):

        kwargs['user_points'] = self.user_points
        kwargs['expire_items'] = self.expire_items

        user = self.get_current_user()

        if user:
            kwargs['list_have'] = self.list_have
            kwargs['list_have'] = self.list_want
            kwargs['tot_matches_items'] = self.tot_matches_items

        self.finish(self.render_string(template, **kwargs))

    def is_header_xml_http_request(self):
        if self.request.headers.get('X-Requested-With') != 'XMLHttpRequest':
            return True
        else:
            return False

    @property
    def request_data(self):
        _dict = DummyDict()
        for param in self.request.arguments:
            _dict[param] = self.get_arguments(param)
        return _dict

    @property
    def user_points(self):
        filters = {'total__gte': '0'}
        queryset = Points.objects.filter(**filters).order_by('-total')[:4]
        data_send = []
        user = self.get_current_user()

        try:
            for x in queryset:
                is_user = False

                if user:
                    is_user = True if user.email == x.user else False

                data_send.append(dict(
                    user=x.user_data.name,
                    total=x.total,
                    status=is_user))

            return sorted(data_send, key=lambda k: int(k['total']),
                reverse=True)
        except:
            return None

    @property
    def expire_items(self):
        queryset = Product.objects.all()
        data_send = []

        try:
            for x in queryset:
                if x.expired:
                    data_send.append(dict(title=x.title, slug=x.slug))[:4]
            return data_send
        except:
            return data_send

    @property
    def list_have(self):
        user = self.get_current_user().email

        try:
            return Product.objects.filter(user=user, type='have')[:4]
        except:
            return None

    @property
    def list_want(self):
        user = self.get_current_user().email
        try:
            return Product.objects.filter(user=user, type='want')[:4]
        except:
            return None

    @property
    def tot_matches_items(self):
        try:
            user = self.get_current_user().email
            return Conversation.tot_notifications_type(user, 'blog')
        except:
            return 0

    def request_file(self, name):
        try:
            return self.request.files[name][0]
        except (KeyError, IndexError):
            return None


class ListMixin(object):

    @property
    def current_page(self):
        current_page = self.get_argument('page', '1')
        return int(current_page) if current_page.isdigit() else 1

    def get_pagination(self, query, count, per_page=10):

        try:
            per_page = int(per_page)
        except ValueError:
            per_page = 10

        page = self.current_page
        paginator = Paginator(page=self.current_page, total_items=count,
                              per_page=per_page)
        per_page = paginator.per_page

        return {
            'items': query[(page - 1) * per_page: page * per_page],
            'pages': paginator.pages,
            'total_items': count,
            'current_page': page,
            'total_pages': paginator.total_pages,
        }
