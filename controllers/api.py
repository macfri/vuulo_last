import logging
import datetime
import settings

from controllers import BaseHandler

from worker.tasks import generate_thumbs_api as generate_thumbs
from libs.utils import validate_date, convert_int, convert_bool, check_email

from models import ApiUser, ApiProduct, Category

log = logging.getLogger(__name__)


class Login(BaseHandler):

    def expiration_date(self):
        return (datetime.datetime.now() + \
                datetime.timedelta(days=2)).strftime(
                    '%Y-%m-%dT%H:%M:%S')

    def get(self, **kwargs):
        self.render('api/login.html', **kwargs)

    def post(self):

        status_code = 0
        email = self.get_argument('email', '')
        password = self.get_argument('password', '')
        token = ''

        if len(email) < 5 or len(password) < 1:
            status_code = 1
        elif not check_email(email):
            status_code = 2
        else:

            if not ApiUser.email_exists(email):

                user = ApiUser()
                user.email = email
                user.password = password
                user.expiration_date = self.expiration_date()
                user.token = ApiUser.generate_token()
                token = user.token

                try:
                    user.save()
                except Exception as exc:
                    log.error(exc)
                    status_code = 3
            else:

                if not ApiUser.auth(email, password):
                    status_code = 4
                    log.info('auth fail!')
                else:
                    user = ApiUser.objects.get(email)
                    user.expiration_date = self.expiration_date()
                    token = user.token

                    try:
                        user.update()
                    except Exception as exc:
                        log.error(exc)
                        status_code = 5

        self.finish(dict(status_code=status_code, token=token))


class AddProduct(BaseHandler):

    def get(self, **kwargs):
        kwargs['action'] = 'new'
        self.render('api/product.html', **kwargs)

    def post(self):

        status_code = 0
        product_id = ''

        token = self.get_argument('token', '')
        title = self.get_argument('title', '')
        description = self.get_argument('description', '')
        cost = convert_int(self.get_argument('cost', ''))
        type = self.get_argument('type', None)

        if settings.DEBUG:
            log.info('token: %s' % token)
            log.info('title: %s' % title)
            log.info('description: %s' % description)
            log.info('cost: %s' % cost)
            log.info('type: %s' % type)

        if len(title) < 1 or len(description) < 1 or len(token) < 1:
            status_code = 1
            log.info('fill data')
        elif not type in (x[0] for x in ApiProduct.TYPES):
            status_code = 2
            log.error('no type')
        else:

            user = ApiUser.get_user_by_token(token)

            if not user:
                log.error('invalid user')
                self.send_error(404)
                return
            else:
                if user.has_expires():
                    status_code = 3
                    log.error('has expires')
                else:

                    _raw_image = self.request_file('filename')

                    if not _raw_image:
                        status_code = 4

        if status_code == 0:

            product = ApiProduct(
                title=title,
                description=description,
                cost=cost,
                type=type,
                user=user.email,
                status='active'
            )

            try:
                product.save()
            except Exception as exc:
                log.error(exc)
                status_code = 5
            else:

                product_id = product.id

                if not product.upload_img(_raw_image.get('body')):
                    status_code = 6
                    log.error('no upload image')
                else:
                    generate_thumbs.delay(product.id)

        log.error('status_code: %s' % status_code)
        self.finish(dict(status_code=status_code,
            product_id=product_id))


class EditProduct(BaseHandler):

    def get(self, id, **kwargs):

        try:
            product = ApiProduct.objects.get(id)
        except:
            self.send_error(404)
            logging.error('invalid obj product')
            return

        if not product.will_travel:
            product.will_travel = 1000

        if not product.location:
            product.location = 'WC 12 avenue'

        if not product.published_at:
            product.published_at = datetime.datetime.now().strftime(
                '%Y-%m-%d %H:%M')

        if not product.expires_at:
            product.expires_at = (datetime.datetime.now() + \
                datetime.timedelta(days=5)).strftime('%Y-%m-%d %H:%M')

        categories = Category.objects.all()

        if not product.categories:
            product.categories = [u'metarials', u'sporting']

        if product.categories:
            for x, y in enumerate(categories):
                if y.title.lower() in product.categories:
                    categories[x].checked = True
                else:
                    categories[x].checked = False

        if settings.DEBUG:
            log.info('image: %s' % product.image)
            log.info('tyny_image: %s' % product.tiny_url)
            log.info('small_image: %s' % product.small_url)
            log.info('large_image: %s' % product.large_url)
            log.info('medium_image: %s' % product.medium_url)
            log.info('is_upload_thumbs: %s' % product.is_upload_thumbs)

        kwargs['product'] = product
        kwargs['categories'] = categories
        kwargs['audiences'] = ApiProduct.AUDIENCES_TYPES
        kwargs['action'] = 'edit'

        self.render('api/product.html', **kwargs)

    def post(self, id):

        status_code = 0

        categories = self.get_arguments('categories', None)
        title = self.get_argument('title', '')
        description = self.get_argument('description', '')

        cost = convert_int(self.get_argument('cost', 0))
        will_travel = convert_int(self.get_argument('will_travel', 0))

        allow_retailers = convert_bool(
            self.get_argument('allow_retailers', None))
        allow_charities = convert_bool(
            self.get_argument('allow_charities', None))
        donate_now = convert_bool(self.get_argument('donate_now', None))

        published_at = self.get_argument('published_at', '')
        expires_at = self.get_argument('expires_at', '')

        location = self.get_argument('location', '')
        audience = self.get_argument('audience', '')
        token = self.get_argument('token', '')
        type = self.get_argument('type', None)

        if settings.DEBUG:
            log.info('token: %s' % token)
            log.info('title: %s' % title)
            log.info('categories: %s' % categories)
            log.info('cost: %s' % cost)
            log.info('will_travel: %s' % will_travel)
            log.info('allow_charities: %s' % allow_charities)
            log.info('allow_retailers: %s' % allow_retailers)
            log.info('donate_now: %s' % donate_now)
            log.info('published_at: %s' % published_at)
            log.info('expires_at: %s' % expires_at)
            log.info('type: %s' % type)
            log.info('location: %s' % location)
            log.info('audience: %s' % audience)

        try:
            product = ApiProduct.objects.get(id)
        except Exception as exc:
            self.send_error(404)
            log.error('invalid obj product')
            return

        if len(title) < 1 or len(description) < 1 or len(token) < 1 or \
                len(published_at) < 1 or len(expires_at) < 1:
            status_code = 1
            log.error('fill data')
        elif not audience in (x[0] for x in ApiProduct.AUDIENCES_TYPES):
            status_code = 1
            log.error('invalid audience')
        elif not type in ApiProduct.TYPES:
            status_code = 2
            log.error('invalid type')
        elif not categories:
            status_code = 7
            log.error('invalid categories')
        elif not validate_date(published_at) or \
                not validate_date(expires_at):
            log.error('invalid published_at or expires_at')
            status_code = 4
        else:
            user = ApiUser.get_user_by_token(token)

            if not user:
                self.send_error(404)
                log.error('invalid user')
                return

            if user.has_expires():
                status_code = 3
                log.error('has expires')
            else:

                _raw_image = self.request_file('filename')

                product.title = title
                product.description = description
                product.cost = cost
                product.will_travel = will_travel

                if type == 'have':
                    product.allow_charities = allow_charities
                    product.donate_now = donate_now
                else:
                    product.allow_retailers = allow_retailers

                product.expires_at = expires_at
                product.published_at = published_at
                product.type = type
                product.user = user.email
                product.categories = categories
                product.location = location
                product.audience = audience
                product.status = 'active'

                try:
                    product.update()
                except Exception as exc:
                    log.error(exc)
                    status_code = 5
                else:

                    if _raw_image:
                        if not product.upload_img(_raw_image.get('body')):
                            status_code = 6
                        else:
                            generate_thumbs.delay(product.id)

        self.finish(dict(status_code=status_code,
            product_id=product.id))
